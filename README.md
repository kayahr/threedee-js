ThreeDee
========

ThreeDee is a small, fast and simple 3D scene graph vector engine for
JavaScript based on the HTML 5 canvas technology.  It was created as a
learning excercise and to prove that JavaScript is fast enough to render
simple 3D scenes.  The project most likely won't receive any updates because
software-rendering is pretty much obsolete since webGL has finally arrived
in the major browsers. But maybe it is still useful for learning basic 3D
techniques like vector and matrix algebra, simple lighting, clipping and
stuff like that.
