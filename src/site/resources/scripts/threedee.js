/**
 * @provide threedee/rendering/FpsCounter.js
 * @provide threedee/Matrix.js
 * @provide threedee/rendering/Frustum.js
 * @provide threedee/rendering/Plane.js
 * @provide threedee/light/PointLight.js
 * @provide threedee/LightNode.js
 * @provide threedee/CameraNode.js
 * @provide threedee/updater/PitchUpdater.js
 * @provide threedee/rendering/RenderPolygon.js
 * @provide threedee/model/Material.js
 * @provide threedee/SceneNode.js
 * @provide threedee/rendering/RenderModel.js
 * @provide threedee/updater/YawUpdater.js
 * @provide threedee/model/Model.js
 * @provide threedee/rendering/TransformedLight.js
 * @provide threedee/Color.js
 * @provide threedee/updater/KeyboardUpdater.js
 * @provide threedee.js
 * @provide threedee/model/Cube.js
 * @provide threedee/light/Light.js
 * @provide threedee/updater/RollUpdater.js
 * @provide threedee/updater/NodeUpdater.js
 * @provide threedee/ModelNode.js
 * @provide threedee/model/Polygon.js
 * @provide threedee/Scene.js
 * @provide threedee/rendering/RenderOptions.js
 * @provide threedee/Vector.js
 * @provide threedee/rendering/PolygonBuffer.js
 */
/*

 ThreeDee - JavaScript 3D scene graph engine
 http://kayahr.github.com/threedee

 Copyright (C) 2009-2011 Klaus Reimer <k@ailis.de>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>

*/
var threedee={};threedee.inherit=function(subClass,superClass){var tmp=superClass.prototype;superClass=new Function;superClass.prototype=tmp;subClass.prototype=new superClass;subClass.prototype.constructor=subClass};threedee.FpsCounter=function(){};
threedee.FpsCounter.prototype.lastResult=0;threedee.FpsCounter.prototype.fps=0;threedee.FpsCounter.prototype.counter=0;threedee.FpsCounter.prototype.frame=function(){var now;now=(new Date).getTime();this.counter++;if(this.lastResult==0)this.lastResult=now;else if(this.lastResult+1E3<now){this.lastResult=now;this.fps=this.counter;this.counter=0}};
threedee.FpsCounter.prototype.getFps=function(){return this.fps};threedee.Matrix=function(){threedee.Matrix.counter++};
threedee.Matrix.counter=0;threedee.Matrix.TMP=new threedee.Matrix;threedee.Matrix.prototype.m00=1;threedee.Matrix.prototype.m01=0;threedee.Matrix.prototype.m02=0;threedee.Matrix.prototype.m03=0;threedee.Matrix.prototype.m10=0;threedee.Matrix.prototype.m11=1;threedee.Matrix.prototype.m12=0;threedee.Matrix.prototype.m13=0;threedee.Matrix.prototype.m20=0;threedee.Matrix.prototype.m21=0;threedee.Matrix.prototype.m22=1;threedee.Matrix.prototype.m23=0;threedee.Matrix.prototype.m30=0;
threedee.Matrix.prototype.m31=0;threedee.Matrix.prototype.m32=0;threedee.Matrix.prototype.m33=1;threedee.Matrix.count=function(){var value=threedee.Matrix.counter;threedee.Matrix.counter=0;return value};
threedee.Matrix.prototype.copy=function(){return(new threedee.Matrix).set(this.m00,this.m01,this.m02,this.m03,this.m10,this.m11,this.m12,this.m13,this.m20,this.m21,this.m22,this.m23,this.m30,this.m31,this.m32,this.m33)};
threedee.Matrix.prototype.set=function(m00,m01,m02,m03,m10,m11,m12,m13,m20,m21,m22,m23,m30,m31,m32,m33){this.m00=m00;this.m01=m01;this.m02=m02;this.m03=m03;this.m10=m10;this.m11=m11;this.m12=m12;this.m13=m13;this.m20=m20;this.m21=m21;this.m22=m22;this.m23=m23;this.m30=m30;this.m31=m31;this.m32=m32;this.m33=m33;return this};
threedee.Matrix.prototype.setIdentity=function(){return this.set(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1)};
threedee.Matrix.prototype.setRotateX=function(angle){var s,c;s=Math.sin(angle);c=Math.cos(angle);return this.set(1,0,0,0,0,c,-s,0,0,s,c,0,0,0,0,1)};
threedee.Matrix.prototype.setRotateY=function(angle){var s,c;s=Math.sin(angle);c=Math.cos(angle);return this.set(c,0,s,0,0,1,0,0,-s,0,c,0,0,0,0,1)};
threedee.Matrix.prototype.setRotateZ=function(angle){var s,c;s=Math.sin(angle);c=Math.cos(angle);return this.set(c,-s,0,0,s,c,0,0,0,0,1,0,0,0,0,1)};
threedee.Matrix.prototype.setScale=function(fx,fy,fz){return this.set(fx,0,0,0,0,fy===undefined?fx:fy,0,0,0,0,fz===undefined?fx:fz,0,0,0,0,1)};
threedee.Matrix.prototype.setScaleX=function(f){return this.set(f,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1)};
threedee.Matrix.prototype.setScaleY=function(f){return this.set(1,0,0,0,0,f,0,0,0,0,1,0,0,0,0,1)};
threedee.Matrix.prototype.setScaleZ=function(f){return this.set(1,0,0,0,0,1,0,0,0,0,f,0,0,0,0,1)};
threedee.Matrix.prototype.setTranslate=function(dx,dy,dz){return this.set(1,0,0,dx,0,1,0,dy,0,0,1,dz,0,0,0,1)};
threedee.Matrix.prototype.setTranslateX=function(d){return this.set(1,0,0,d,0,1,0,0,0,0,1,0,0,0,0,1)};
threedee.Matrix.prototype.setTranslateY=function(d){return this.set(1,0,0,0,0,1,0,d,0,0,1,0,0,0,0,1)};
threedee.Matrix.prototype.setTranslateZ=function(d){return this.set(1,0,0,0,0,1,0,0,0,0,1,d,0,0,0,1)};
threedee.Matrix.prototype.transform=function(m){return this.set(this.m00*m.m00+this.m01*m.m10+this.m02*m.m20+this.m03*m.m30,this.m00*m.m01+this.m01*m.m11+this.m02*m.m21+this.m03*m.m31,this.m00*m.m02+this.m01*m.m12+this.m02*m.m22+this.m03*m.m32,this.m00*m.m03+this.m01*m.m13+this.m02*m.m23+this.m03*m.m33,this.m10*m.m00+this.m11*m.m10+this.m12*m.m20+this.m13*m.m30,this.m10*m.m01+this.m11*m.m11+this.m12*m.m21+this.m13*m.m31,this.m10*m.m02+this.m11*m.m12+this.m12*m.m22+this.m13*m.m32,this.m10*m.m03+this.m11*
m.m13+this.m12*m.m23+this.m13*m.m33,this.m20*m.m00+this.m21*m.m10+this.m22*m.m20+this.m23*m.m30,this.m20*m.m01+this.m21*m.m11+this.m22*m.m21+this.m23*m.m31,this.m20*m.m02+this.m21*m.m12+this.m22*m.m22+this.m23*m.m32,this.m20*m.m03+this.m21*m.m13+this.m22*m.m23+this.m23*m.m33,this.m30*m.m00+this.m31*m.m10+this.m32*m.m20+this.m33*m.m30,this.m30*m.m01+this.m31*m.m11+this.m32*m.m21+this.m33*m.m31,this.m30*m.m02+this.m31*m.m12+this.m32*m.m22+this.m33*m.m32,this.m30*m.m03+this.m31*m.m13+this.m32*m.m23+
this.m33*m.m33)};
threedee.Matrix.prototype.multiply=function(f){this.m00*=f;this.m01*=f;this.m02*=f;this.m03*=f;this.m10*=f;this.m11*=f;this.m12*=f;this.m13*=f;this.m20*=f;this.m21*=f;this.m22*=f;this.m23*=f;this.m30*=f;this.m31*=f;this.m32*=f;this.m33*=f;return this};
threedee.Matrix.prototype.divide=function(f){this.m00/=f;this.m01/=f;this.m02/=f;this.m03/=f;this.m10/=f;this.m11/=f;this.m12/=f;this.m13/=f;this.m20/=f;this.m21/=f;this.m22/=f;this.m23/=f;this.m30/=f;this.m31/=f;this.m32/=f;this.m33/=f;return this};
threedee.Matrix.prototype.translate=function(dx,dy,dz){return this.transform(threedee.Matrix.TMP.setTranslate(dx,dy,dz))};
threedee.Matrix.prototype.translateX=function(d){return this.transform(threedee.Matrix.TMP.setTranslateX(d))};
threedee.Matrix.prototype.translateY=function(d){return this.transform(threedee.Matrix.TMP.setTranslateY(d))};
threedee.Matrix.prototype.translateZ=function(d){return this.transform(threedee.Matrix.TMP.setTranslateZ(d))};
threedee.Matrix.prototype.scale=function(fx,fy,fz){return this.transform(threedee.Matrix.TMP.setScale(fx,fy===undefined?fx:fy,fz===undefined?fx:fz))};
threedee.Matrix.prototype.scaleX=function(f){return this.transform(threedee.Matrix.TMP.setScaleX(f))};
threedee.Matrix.prototype.scaleY=function(f){return this.transform(threedee.Matrix.TMP.setScaleY(f))};
threedee.Matrix.prototype.scaleZ=function(f){return this.transform(threedee.Matrix.TMP.setScaleZ(f))};
threedee.Matrix.prototype.rotateX=function(r){return this.transform(threedee.Matrix.TMP.setRotateX(r))};
threedee.Matrix.prototype.rotateY=function(r){return this.transform(threedee.Matrix.TMP.setRotateY(r))};
threedee.Matrix.prototype.rotateZ=function(r){return this.transform(threedee.Matrix.TMP.setRotateZ(r))};
threedee.Matrix.prototype.getDeterminant=function(){return this.m03*this.m12*this.m21*this.m30-this.m02*this.m13*this.m21*this.m30-this.m03*this.m11*this.m22*this.m30+this.m01*this.m13*this.m22*this.m30+this.m02*this.m11*this.m23*this.m30-this.m01*this.m12*this.m23*this.m30-this.m03*this.m12*this.m20*this.m31+this.m02*this.m13*this.m20*this.m31+this.m03*this.m10*this.m22*this.m31-this.m00*this.m13*this.m22*this.m31-this.m02*this.m10*this.m23*this.m31+this.m00*this.m12*this.m23*this.m31+this.m03*this.m11*
this.m20*this.m32-this.m01*this.m13*this.m20*this.m32-this.m03*this.m10*this.m21*this.m32+this.m00*this.m13*this.m21*this.m32+this.m01*this.m10*this.m23*this.m32-this.m00*this.m11*this.m23*this.m32-this.m02*this.m11*this.m20*this.m33+this.m01*this.m12*this.m20*this.m33+this.m02*this.m10*this.m21*this.m33-this.m00*this.m12*this.m21*this.m33-this.m01*this.m10*this.m22*this.m33+this.m00*this.m11*this.m22*this.m33};
threedee.Matrix.prototype.invert=function(){return this.set(this.m12*this.m23*this.m31-this.m13*this.m22*this.m31+this.m13*this.m21*this.m32-this.m11*this.m23*this.m32-this.m12*this.m21*this.m33+this.m11*this.m22*this.m33,this.m03*this.m22*this.m31-this.m02*this.m23*this.m31-this.m03*this.m21*this.m32+this.m01*this.m23*this.m32+this.m02*this.m21*this.m33-this.m01*this.m22*this.m33,this.m02*this.m13*this.m31-this.m03*this.m12*this.m31+this.m03*this.m11*this.m32-this.m01*this.m13*this.m32-this.m02*
this.m11*this.m33+this.m01*this.m12*this.m33,this.m03*this.m12*this.m21-this.m02*this.m13*this.m21-this.m03*this.m11*this.m22+this.m01*this.m13*this.m22+this.m02*this.m11*this.m23-this.m01*this.m12*this.m23,this.m13*this.m22*this.m30-this.m12*this.m23*this.m30-this.m13*this.m20*this.m32+this.m10*this.m23*this.m32+this.m12*this.m20*this.m33-this.m10*this.m22*this.m33,this.m02*this.m23*this.m30-this.m03*this.m22*this.m30+this.m03*this.m20*this.m32-this.m00*this.m23*this.m32-this.m02*this.m20*this.m33+
this.m00*this.m22*this.m33,this.m03*this.m12*this.m30-this.m02*this.m13*this.m30-this.m03*this.m10*this.m32+this.m00*this.m13*this.m32+this.m02*this.m10*this.m33-this.m00*this.m12*this.m33,this.m02*this.m13*this.m20-this.m03*this.m12*this.m20+this.m03*this.m10*this.m22-this.m00*this.m13*this.m22-this.m02*this.m10*this.m23+this.m00*this.m12*this.m23,this.m11*this.m23*this.m30-this.m13*this.m21*this.m30+this.m13*this.m20*this.m31-this.m10*this.m23*this.m31-this.m11*this.m20*this.m33+this.m10*this.m21*
this.m33,this.m03*this.m21*this.m30-this.m01*this.m23*this.m30-this.m03*this.m20*this.m31+this.m00*this.m23*this.m31+this.m01*this.m20*this.m33-this.m00*this.m21*this.m33,this.m01*this.m13*this.m30-this.m03*this.m11*this.m30+this.m03*this.m10*this.m31-this.m00*this.m13*this.m31-this.m01*this.m10*this.m33+this.m00*this.m11*this.m33,this.m03*this.m11*this.m20-this.m01*this.m13*this.m20-this.m03*this.m10*this.m21+this.m00*this.m13*this.m21+this.m01*this.m10*this.m23-this.m00*this.m11*this.m23,this.m12*
this.m21*this.m30-this.m11*this.m22*this.m30-this.m12*this.m20*this.m31+this.m10*this.m22*this.m31+this.m11*this.m20*this.m32-this.m10*this.m21*this.m32,this.m01*this.m22*this.m30-this.m02*this.m21*this.m30+this.m02*this.m20*this.m31-this.m00*this.m22*this.m31-this.m01*this.m20*this.m32+this.m00*this.m21*this.m32,this.m02*this.m11*this.m30-this.m01*this.m12*this.m30-this.m02*this.m10*this.m31+this.m00*this.m12*this.m31+this.m01*this.m10*this.m32-this.m00*this.m11*this.m32,this.m01*this.m12*this.m20-
this.m02*this.m11*this.m20+this.m02*this.m10*this.m21-this.m00*this.m12*this.m21-this.m01*this.m10*this.m22+this.m00*this.m11*this.m22).divide(this.getDeterminant())};threedee.Frustum=function(width,height,scale,far,near){var xAngle,yAngle,sh,sv,ch,cv;xAngle=Math.atan2(width/2,scale)-1.0E-4;yAngle=Math.atan2(height/2,scale)-1.0E-4;sh=Math.sin(xAngle);sv=Math.sin(yAngle);ch=Math.cos(xAngle);cv=Math.cos(yAngle);this.left=new threedee.Plane(new threedee.Vector(ch,0,sh),0);this.right=new threedee.Plane(new threedee.Vector(-ch,0,sh),0);this.top=new threedee.Plane(new threedee.Vector(0,-cv,sv),0);this.bottom=new threedee.Plane(new threedee.Vector(0,cv,sv),0);this.near=
new threedee.Plane(new threedee.Vector(0,0,1),-(near?near:10));if(far)this.far=new threedee.Plane(new threedee.Vector(0,0,-1),-far)};
threedee.Frustum.prototype.left;threedee.Frustum.prototype.right;threedee.Frustum.prototype.top;threedee.Frustum.prototype.bottom;threedee.Frustum.prototype.near;threedee.Frustum.prototype.far;threedee.Frustum.prototype.clipPolygon=function(polygon){if(polygon.clip(this.near))return true;if(this.far)if(polygon.clip(this.far))return true;if(polygon.clip(this.left))return true;if(polygon.clip(this.right))return true;if(polygon.clip(this.top))return true;if(polygon.clip(this.bottom))return true;return false};threedee.Plane=function(normal,distance){this.normal=normal;this.distance=distance};
threedee.Plane.prototype.normal;threedee.Plane.prototype.distance;threedee.Plane.prototype.getNormal=function(){return this.normal};
threedee.Plane.prototype.getDistance=function(){return this.distance};threedee.Vector=function(x,y,z){if(x)this.x=x;if(y)this.y=y;if(z)this.z=z;threedee.Vector.counter++};
threedee.Vector.counter=0;threedee.Vector.prototype.x=0;threedee.Vector.prototype.y=0;threedee.Vector.prototype.z=0;threedee.Vector.count=function(){var value=threedee.Vector.counter;threedee.Vector.counter=0;return value};
threedee.Vector.prototype.copy=function(v){return(v?v:new threedee.Vector).set(this.x,this.y,this.z)};
threedee.Vector.prototype.set=function(x,y,z){this.x=x;this.y=y;this.z=z;return this};
threedee.Vector.prototype.add=function(v){this.x+=v.x;this.y+=v.y;this.z+=v.z;return this};
threedee.Vector.prototype.sub=function(v){this.x-=v.x;this.y-=v.y;this.z-=v.z;return this};
threedee.Vector.prototype.scale=function(fx,fy,fz){this.x*=fx;this.y*=fy===undefined?fx:fy;this.z*=fy===undefined?fx:fz;return this};
threedee.Vector.prototype.dot=function(v){return this.x*v.x+this.y*v.y+this.z*v.z};
threedee.Vector.prototype.cross=function(v){var x,y,z;x=this.y*v.z-this.z*v.y;y=this.z*v.x-this.x*v.z;z=this.x*v.y-this.y*v.x;this.x=x;this.y=y;this.z=z;return this};
threedee.Vector.prototype.length=function(){return Math.sqrt(this.x*this.x+this.y*this.y+this.z*this.z)};
threedee.Vector.prototype.toUnit=function(){var len;len=this.length();this.x/=len;this.y/=len;this.z/=len;return this};
threedee.Vector.prototype.getAngle=function(v){return Math.acos(this.copy().toUnit().dot(v.toUnit()))};
threedee.Vector.prototype.transform=function(m){return this.set(m.m00*this.x+m.m01*this.y+m.m02*this.z+m.m03,m.m10*this.x+m.m11*this.y+m.m12*this.z+m.m13,m.m20*this.x+m.m21*this.y+m.m22*this.z+m.m23)};
threedee.Vector.prototype.toJSON=function(){return{"x":this.x,"y":this.y,"z":this.z}};
threedee.Vector.fromJSON=function(data){if(!data)return null;return new threedee.Vector(+data["x"],+data["y"],+data["z"])};threedee.Color=function(red,green,blue){if(red)this.red=red;if(green)this.green=green;if(blue)this.blue=blue;this.css="rgb("+this.red*255+","+this.green*255+","+this.blue*255+")";threedee.Color.counter++};
threedee.Color.counter=0;threedee.Color.prototype.red=0;threedee.Color.prototype.green=0;threedee.Color.prototype.blue=0;threedee.Color.prototype.css="rgb(0,0,0)";threedee.Color.BLACK=new threedee.Color(0,0,0);threedee.Color.RED=new threedee.Color(1,0,0);threedee.Color.GREEN=new threedee.Color(0,1,0);threedee.Color.BLUE=new threedee.Color(0,0,1);threedee.Color.DARK_GRAY=new threedee.Color(0.25,0.25,0.25);threedee.Color.YELLOW=new threedee.Color(1,1,0);threedee.Color.WHITE=new threedee.Color(1,1,1);
threedee.Color.count=function(){var value=threedee.Color.counter;threedee.Color.counter=0;return value};
threedee.Color.prototype.getRed=function(){return this.red};
threedee.Color.prototype.getGreen=function(){return this.green};
threedee.Color.prototype.getBlue=function(){return this.blue};
threedee.Color.prototype.toCSS=function(){return this.css};
threedee.Color.prototype.getComponents=function(){return[this.red,this.green,this.blue]};
threedee.Color.prototype.getComponent=function(component){return component?component==1?this.green:this.blue:this.red};
threedee.Color.prototype.toJSON=function(){return{"r":this.red,"g":this.green,"b":this.blue}};
threedee.Color.fromJSON=function(data){if(!data)return null;return new threedee.Color(+data["r"],+data["g"],+data["b"])};threedee.Light=function(){};threedee.PointLight=function(color){if(color)this.color=color};
threedee.inherit(threedee.PointLight,threedee.Light);threedee.PointLight.prototype.color=threedee.Color.WHITE;threedee.PointLight.prototype.setColor=function(color){this.color=color};
threedee.PointLight.prototype.getColor=function(){return this.color};threedee.SceneNode=function(){this.transform=new threedee.Matrix;this.updaters=[]};
threedee.SceneNode.prototype.updaters;threedee.SceneNode.prototype.parentNode=null;threedee.SceneNode.prototype.nextSibling=null;threedee.SceneNode.prototype.previousSibling=null;threedee.SceneNode.prototype.firstChild=null;threedee.SceneNode.prototype.lastChild=null;threedee.SceneNode.prototype.transform;
threedee.SceneNode.prototype.appendChild=function(node){var oldParent;if(!node)throw new Error("node must not be null");if(node==this)throw new Error("node can not be a child of itself");oldParent=node.parentNode;if(oldParent)oldParent.removeChild(node);node.previousSibling=this.lastChild;if(this.lastChild)this.lastChild.nextSibling=node;this.lastChild=node;if(!this.firstChild)this.firstChild=node;node.parentNode=this};
threedee.SceneNode.prototype.getFirstChild=function(){return this.firstChild};
threedee.SceneNode.prototype.getLastChild=function(){return this.lastChild};
threedee.SceneNode.prototype.getNextSibling=function(){return this.nextSibling};
threedee.SceneNode.prototype.getParentNode=function(){return this.parentNode};
threedee.SceneNode.prototype.getPreviousSibling=function(){return this.previousSibling};
threedee.SceneNode.prototype.hasChildNodes=function(){return!!this.firstChild};
threedee.SceneNode.prototype.insertBefore=function(insertNode,referenceNode){var oldParent,oldPrevious;if(!insertNode)throw new Error("newNode must be set");if(!referenceNode)throw new Error("referenceNode must be set");if(insertNode==this)throw new Error("newNode can not be a child of itself");if(referenceNode.parentNode!=this)throw new Error("Reference node is not my child node");oldParent=insertNode.parentNode;if(oldParent)oldParent.removeChild(insertNode);oldPrevious=referenceNode.previousSibling;
if(oldPrevious)oldPrevious.nextSibling=insertNode;else this.firstChild=insertNode;referenceNode.previousSibling=insertNode;insertNode.previousSibling=oldPrevious;insertNode.nextSibling=referenceNode;insertNode.parentNode=this};
threedee.SceneNode.prototype.removeChild=function(node){var next,prev;if(!node)throw new Error("node must be set");if(node.parentNode!=this)throw new Error("node is not my child node");next=node.nextSibling;prev=node.previousSibling;if(next)next.previousSibling=prev;if(prev)prev.nextSibling=next;if(node==this.firstChild)this.firstChild=next;if(node==this.lastChild)this.lastChild=prev;node.parentNode=null;node.nextSibling=null;node.previousSibling=null};
threedee.SceneNode.prototype.replaceChild=function(oldNode,newNode){var next;if(!oldNode)throw new Error("oldNode must be set");if(!newNode)throw new Error("newNode must be set");if(newNode==this)throw new Error("node can not be a child of itself");if(oldNode.parentNode!=this)throw new Error("node is not my child node");if(newNode!=oldNode){next=oldNode.nextSibling;this.removeChild(oldNode);if(next==null)this.appendChild(newNode);else this.insertBefore(newNode,next)}};
threedee.SceneNode.prototype.update=function(delta){var childNode,i,max;for(i=0,max=this.updaters.length;i<max;i++)this.updaters[i].update(this,delta);childNode=this.firstChild;while(childNode){childNode.update(delta);childNode=childNode.getNextSibling()}};
threedee.SceneNode.prototype.render=function(buffer,transform){var childNode;childNode=this.firstChild;while(childNode){childNode.render(buffer,transform.copy().transform(childNode.getTransform()));childNode=childNode.getNextSibling()}};
threedee.SceneNode.prototype.getEffectiveTransform=function(){if(this.parentNode!=null)return this.parentNode.getEffectiveTransform().copy().transform(this.transform);else return this.transform.copy()};
threedee.SceneNode.prototype.getTransform=function(){return this.transform};
threedee.SceneNode.prototype.setTransform=function(transform){if(!transform)throw new Error("transform must be set");this.transform=transform};
threedee.SceneNode.prototype.addUpdater=function(updater){this.updaters.push(updater)};
threedee.SceneNode.prototype.removeUpdater=function(updater){var i;for(i=this.updaters.length-1;i>=0;i--)if(this.updaters[i]==updater){this.updaters.splice(i,1);break}};threedee.LightNode=function(light){threedee.SceneNode.call(this);this.light=light};
threedee.inherit(threedee.LightNode,threedee.SceneNode);threedee.LightNode.prototype.light;threedee.LightNode.prototype.render=function(buffer,transform){buffer.addLight(this.light,transform)};threedee.CameraNode=function(){threedee.SceneNode.call(this)};
threedee.inherit(threedee.CameraNode,threedee.SceneNode);
threedee.CameraNode.prototype.lookAt=function(eye,center,up){var forwardx,forwardy,forwardz,invMag,upx,upy,upz,sidex,sidey,sidez,mat;forwardx=eye.x-center.x;forwardy=eye.y-center.y;forwardz=eye.z-center.z;invMag=1/Math.sqrt(forwardx*forwardx+forwardy*forwardy+forwardz*forwardz);forwardx=forwardx*invMag;forwardy=forwardy*invMag;forwardz=forwardz*invMag;invMag=1/Math.sqrt(up.x*up.x+up.y*up.y+up.z*up.z);upx=up.x*invMag;upy=up.y*invMag;upz=up.z*invMag;sidex=upy*forwardz-forwardy*upz;sidey=upz*forwardx-
upx*forwardz;sidez=upx*forwardy-upy*forwardx;invMag=1/Math.sqrt(sidex*sidex+sidey*sidey+sidez*sidez);sidex*=invMag;sidey*=invMag;sidez*=invMag;upx=forwardy*sidez-sidey*forwardz;upy=forwardz*sidex-forwardx*sidez;upz=forwardx*sidey-forwardy*sidex;mat=[];mat[0]=sidex;mat[1]=sidey;mat[2]=sidez;mat[4]=upx;mat[5]=upy;mat[6]=upz;mat[8]=forwardx;mat[9]=forwardy;mat[10]=forwardz;mat[3]=-eye.x*mat[0]+-eye.y*mat[1]+-eye.z*mat[2];mat[7]=-eye.x*mat[4]+-eye.y*mat[5]+-eye.z*mat[6];mat[11]=-eye.x*mat[8]+-eye.y*mat[9]+
-eye.z*mat[10];mat[12]=mat[13]=mat[14]=0;mat[15]=1;var m=(new threedee.Matrix).set(mat[0],mat[1],mat[2],mat[3],mat[4],mat[5],mat[6],mat[7],mat[8],mat[9],mat[10],mat[11],mat[12],mat[13],mat[14],mat[15]);this.setTransform(m)};threedee.NodeUpdater=function(){};
threedee.NodeUpdater.prototype.update=function(node,delta){};threedee.PitchUpdater=function(angle){threedee.NodeUpdater.call(this);this.angle=angle};
threedee.inherit(threedee.PitchUpdater,threedee.NodeUpdater);threedee.PitchUpdater.prototype.angle;threedee.PitchUpdater.prototype.getAngle=function(){return this.angle};
threedee.PitchUpdater.prototype.setAngle=function(angle){this.angle=angle};
threedee.PitchUpdater.prototype.update=function(node,delta){if(this.angle==0)return;node.getTransform().rotateX(this.angle*delta/1E3)};threedee.RenderPolygon=function(model,polygon,vertices){var i,max;this.model=model;this.material=polygon.getMaterial();this.vertices=[];this.size=polygon.countVertices();this.origSize=this.size;for(i=0,max=this.size;i<max;i++)this.vertices.push(vertices[polygon.getVertex(i)]);this.polygon=[];this.prevPolygon=[]};
threedee.RenderPolygon.V1=new threedee.Vector;threedee.RenderPolygon.V2=new threedee.Vector;threedee.RenderPolygon.prototype.vertices;threedee.RenderPolygon.prototype.size=0;threedee.RenderPolygon.prototype.origSize=0;threedee.RenderPolygon.prototype.extras=0;threedee.RenderPolygon.prototype.polygon;threedee.RenderPolygon.prototype.prevPolygon;threedee.RenderPolygon.prototype.normal=null;threedee.RenderPolygon.prototype.center=null;threedee.RenderPolygon.prototype.vertices;
threedee.RenderPolygon.prototype.averageZ=0;threedee.RenderPolygon.prototype.material=null;threedee.RenderPolygon.prototype.model;threedee.RenderPolygon.prototype.init=function(){var i;this.size=this.origSize;for(i=this.size-1;i>=0;i--)this.polygon[i]=i;this.extras=0};
threedee.RenderPolygon.prototype.clip=function(plane){var max,j,i,a,b,c,tmp,planeNormal,planeDistance,distanceA,distanceB,s,va,vb;tmp=this.prevPolygon;this.prevPolygon=this.polygon;this.polygon=tmp;max=this.size;j=0;planeNormal=plane.getNormal();planeDistance=plane.getDistance();for(i=0;i<max;i++){a=this.prevPolygon[i];b=this.prevPolygon[(i+1)%max];va=this.vertices[a];vb=this.vertices[b];distanceA=va.dot(planeNormal)-planeDistance;distanceB=vb.dot(planeNormal)-planeDistance;if(distanceA<0&&distanceB<
0)continue;if(distanceA>=0&&distanceB>=0)this.polygon[j++]=a;else{s=distanceA/(distanceA-distanceB);c=this.origSize+this.extras++;if(!this.vertices[c])this.vertices[c]=new threedee.Vector;this.vertices[c].set(va.x+s*(vb.x-va.x),va.y+s*(vb.y-va.y),va.z+s*(vb.z-va.z));if(distanceA<0)this.polygon[j++]=c;else{this.polygon[j++]=a;this.polygon[j++]=c}}}this.size=j;return!j};
threedee.RenderPolygon.prototype.countVertices=function(){return this.size};
threedee.RenderPolygon.prototype.getVertex=function(index){return this.vertices[this.polygon[index]]};
threedee.RenderPolygon.prototype.getCenter=function(){var vertexCount,ax,ay,az,i,v;vertexCount=this.size;ax=0;ay=0;az=0;for(i=0;i<vertexCount;i++){v=this.vertices[this.polygon[i]];ax+=v.x;ay+=v.y;az+=v.z}ax/=vertexCount;ay/=vertexCount;az/=vertexCount;if(!this.center)this.center=new threedee.Vector;return this.center.set(ax,ay,az)};
threedee.RenderPolygon.prototype.getNormal=function(){var a,b,c,v1,v2;if(this.polygon.length<3)return null;a=this.vertices[this.polygon[0]];b=this.vertices[this.polygon[1]];c=this.vertices[this.polygon[2]];v1=b.copy(threedee.RenderPolygon.V1).sub(a);v2=a.copy(threedee.RenderPolygon.V2).sub(c);this.normal=v1.cross(v2).toUnit().copy(this.normal);return this.normal};
threedee.RenderPolygon.prototype.updateAverageZ=function(){var averageZ,vertexCount,v;averageZ=0;vertexCount=this.size;for(v=0;v<vertexCount;v++)averageZ+=this.vertices[this.polygon[v]].z;averageZ/=vertexCount;this.averageZ=averageZ};
threedee.RenderPolygon.compare=function(a,b){return b.averageZ-a.averageZ};
threedee.RenderPolygon.prototype.isBackface=function(){return this.getNormal().dot(this.getVertex(0))>0};
threedee.RenderPolygon.prototype.getMaterial=function(){if(this.material)return this.material;return this.model.getMaterial()};threedee.Material=function(ambient,diffuse,emissive){if(ambient)this.ambient=ambient;if(diffuse)this.diffuse=diffuse;if(emissive)this.emissive=emissive;threedee.Material.counter++};
threedee.Material.counter=0;threedee.Material.DEFAULT=new threedee.Material;threedee.Material.prototype.ambient=threedee.Color.WHITE;threedee.Material.prototype.diffuse=threedee.Color.WHITE;threedee.Material.prototype.emissive=threedee.Color.BLACK;threedee.Material.count=function(){var value=threedee.Material.counter;threedee.Material.counter=0;return value};
threedee.Material.prototype.getAmbient=function(){return this.ambient};
threedee.Material.prototype.getDiffuse=function(){return this.diffuse};
threedee.Material.prototype.getEmissive=function(){return this.emissive};
threedee.Material.prototype.toJSON=function(){return{"a":this.ambient.toJSON(),"d":this.diffuse.toJSON(),"e":this.emissive.toJSON()}};
threedee.Material.fromJSON=function(data){if(!data)return null;return new threedee.Material(threedee.Color.fromJSON(data["a"]),threedee.Color.fromJSON(data["d"]),threedee.Color.fromJSON(data["e"]))};threedee.RenderModel=function(model){var i,max;this.model=model;this.vertices=[];for(i=0,max=this.model.countVertices();i<max;i++)this.vertices.push(this.model.getVertex(i).copy());this.polygons=[];for(i=0,max=this.model.countPolygons();i<max;i++)this.polygons.push(new threedee.RenderPolygon(model,this.model.getPolygon(i),this.vertices))};
threedee.RenderModel.prototype.vertices;threedee.RenderModel.prototype.polygons;threedee.RenderModel.prototype.model;threedee.RenderModel.prototype.transform=function(transform){var i,max;for(i=0,max=this.vertices.length;i<max;i++)this.model.getVertex(i).copy(this.vertices[i]).transform(transform)};
threedee.RenderModel.prototype.countPolygons=function(){return this.polygons.length};
threedee.RenderModel.prototype.getPolygon=function(index){return this.polygons[index]};threedee.YawUpdater=function(angle){threedee.NodeUpdater.call(this);this.angle=angle};
threedee.inherit(threedee.YawUpdater,threedee.NodeUpdater);threedee.YawUpdater.prototype.angle;threedee.YawUpdater.prototype.getAngle=function(){return this.angle};
threedee.YawUpdater.prototype.setAngle=function(angle){this.angle=angle};
threedee.YawUpdater.prototype.update=function(node,delta){if(this.angle==0)return;node.getTransform().rotateY(this.angle*delta/1E3)};threedee.Model=function(vertices,polygons,material){this.vertices=vertices;this.polygons=polygons;if(material)this.material=material};
threedee.Model.prototype.vertices;threedee.Model.prototype.polygons;threedee.Model.prototype.material=threedee.Material.DEFAULT;threedee.Model.prototype.countVertices=function(){return this.vertices.length};
threedee.Model.prototype.getVertex=function(index){return this.vertices[index]};
threedee.Model.prototype.countPolygons=function(){return this.polygons.length};
threedee.Model.prototype.getPolygon=function(index){return this.polygons[index]};
threedee.Model.prototype.getMaterial=function(){return this.material};
threedee.Model.prototype.toJSON=function(){var data,vertices,polygons,i,max;vertices=[];polygons=[];for(i=0,max=this.vertices.length;i<max;i++)vertices.push(this.vertices[i].toJSON());for(i=0,max=this.polygons.length;i<max;i++)polygons.push(this.polygons[i].toJSON());data={"v":vertices,"p":polygons};if(this.material)data.m=this.material.toJSON();return data};
threedee.Model.fromJSON=function(data){var material,i,max,vertices,polygons;polygons=[];for(i=0,max=data.p.length;i<max;i++)polygons.push(threedee.Polygon.fromJSON(data.p[i]));vertices=[];for(i=0,max=data.v.length;i<max;i++)vertices.push(threedee.Vector.fromJSON(data.v[i]));material=threedee.Material.fromJSON(data.m);return new threedee.Model(vertices,polygons,material)};threedee.Polygon=function(vertices,material){this.vertices=vertices;if(material)this.material=material;threedee.Polygon.counter++};
threedee.Polygon.counter=0;threedee.Polygon.prototype.vertices;threedee.Polygon.prototype.material=null;threedee.Polygon.count=function(){var value=threedee.Polygon.counter;threedee.Polygon.counter=0;return value};
threedee.Polygon.prototype.countVertices=function(){return this.vertices.length};
threedee.Polygon.prototype.getVertex=function(index){return this.vertices[index]};
threedee.Polygon.prototype.getMaterial=function(){return this.material};
threedee.Polygon.prototype.setMaterial=function(material){this.material=material};
threedee.Polygon.prototype.toJSON=function(){var data;data={"v":this.vertices};if(this.material)data.m=this.material.toJSON();return data};
threedee.Polygon.fromJSON=function(data){if(!data)return null;return new threedee.Polygon(data["v"],threedee.Material.fromJSON(data["m"]))};threedee.TransformedLight=function(light,transform){this.light=light;this.position=(new threedee.Vector).transform(transform)};
threedee.TransformedLight.prototype.light;threedee.TransformedLight.prototype.position;threedee.TransformedLight.prototype.getLight=function(){return this.light};
threedee.TransformedLight.prototype.getPosition=function(){return this.position};threedee.KeyboardUpdater=function(element){var updater;threedee.NodeUpdater.call(this);if(!element)element=window;this.element=element;updater=this;element.addEventListener("keydown",this.handleKeyDown.bind(this),false);element.addEventListener("keyup",this.handleKeyUp.bind(this),false)};
threedee.inherit(threedee.KeyboardUpdater,threedee.NodeUpdater);threedee.KeyboardUpdater.prototype.element;threedee.KeyboardUpdater.prototype.speed=10;threedee.KeyboardUpdater.prototype.rotSpeed=45*Math.PI/180;threedee.KeyboardUpdater.prototype.left=false;threedee.KeyboardUpdater.prototype.right=false;threedee.KeyboardUpdater.prototype.up=false;threedee.KeyboardUpdater.prototype.down=false;threedee.KeyboardUpdater.prototype.forward=false;threedee.KeyboardUpdater.prototype.backward=false;
threedee.KeyboardUpdater.prototype.pitchUp=false;threedee.KeyboardUpdater.prototype.pitchDown=false;threedee.KeyboardUpdater.prototype.yawLeft=false;threedee.KeyboardUpdater.prototype.yawRight=false;threedee.KeyboardUpdater.prototype.rollLeft=false;threedee.KeyboardUpdater.prototype.rollRight=false;
threedee.KeyboardUpdater.prototype.handleKeyDown=function(e){switch(e.keyCode){case 87:this.forward=true;break;case 83:this.backward=true;break;case 65:this.left=true;break;case 68:this.right=true;break;case 82:this.down=true;break;case 70:this.up=true;break;case 37:this.yawLeft=true;break;case 39:this.yawRight=true;break;case 38:this.pitchUp=true;break;case 40:this.pitchDown=true;break;case 81:this.rollLeft=true;break;case 69:this.rollRight=true;break;default:return}e.preventDefault()};
threedee.KeyboardUpdater.prototype.handleKeyUp=function(e){switch(e.keyCode){case 87:this.forward=false;break;case 83:this.backward=false;break;case 65:this.left=false;break;case 68:this.right=false;break;case 82:this.down=false;break;case 70:this.up=false;break;case 37:this.yawLeft=false;break;case 39:this.yawRight=false;break;case 38:this.pitchUp=false;break;case 40:this.pitchDown=false;break;case 81:this.rollLeft=false;break;case 69:this.rollRight=false;break;default:}e.preventDefault()};
threedee.KeyboardUpdater.prototype.update=function(node,delta){var x,y,z,rx,ry,rz,transform;x=this.right?1:this.left?-1:0;y=this.down?1:this.up?-1:0;z=this.forward?1:this.backward?-1:0;rx=this.pitchUp?1:this.pitchDown?-1:0;ry=this.yawRight?1:this.yawLeft?-1:0;rz=this.rollLeft?1:this.rollRight?-1:0;transform=node.getTransform();transform.translate(x*this.speed*delta/1E3,y*this.speed*delta/1E3,z*this.speed*delta/1E3);transform.rotateX(rx*this.rotSpeed*delta/1E3);transform.rotateY(ry*this.rotSpeed*delta/
1E3);transform.rotateZ(rz*this.rotSpeed*delta/1E3)};threedee.Cube=function(xRadius,yRadius,zRadius){if(yRadius===undefined)yRadius=xRadius;if(zRadius===undefined)zRadius=xRadius;threedee.Model.call(this,[new threedee.Vector(-xRadius,yRadius,zRadius),new threedee.Vector(-xRadius,yRadius,-zRadius),new threedee.Vector(xRadius,yRadius,-zRadius),new threedee.Vector(xRadius,yRadius,zRadius),new threedee.Vector(-xRadius,-yRadius,zRadius),new threedee.Vector(-xRadius,-yRadius,-zRadius),new threedee.Vector(xRadius,-yRadius,-zRadius),new threedee.Vector(xRadius,
-yRadius,zRadius)],[new threedee.Polygon([0,1,2,3]),new threedee.Polygon([7,3,2,6]),new threedee.Polygon([4,7,6,5]),new threedee.Polygon([1,0,4,5]),new threedee.Polygon([0,3,7,4]),new threedee.Polygon([6,2,1,5])])};
threedee.inherit(threedee.Cube,threedee.Model);threedee.RollUpdater=function(angle){threedee.NodeUpdater.call(this);this.angle=angle};
threedee.inherit(threedee.RollUpdater,threedee.NodeUpdater);threedee.RollUpdater.prototype.angle;threedee.RollUpdater.prototype.getAngle=function(){return this.angle};
threedee.RollUpdater.prototype.setAngle=function(angle){this.angle=angle};
threedee.RollUpdater.prototype.update=function(node,delta){if(this.angle==0)return;node.getTransform().rotateZ(this.angle*delta/1E3)};threedee.ModelNode=function(model){threedee.SceneNode.call(this);this.model=new threedee.RenderModel(model)};
threedee.inherit(threedee.ModelNode,threedee.SceneNode);threedee.ModelNode.prototype.model;threedee.ModelNode.prototype.render=function(buffer,transform){buffer.addModel(this.model,transform);threedee.SceneNode.prototype.render.call(this,buffer,transform)};threedee.Scene=function(){this.buffer=new threedee.PolygonBuffer};
threedee.Scene.prototype.rootNode=null;threedee.Scene.prototype.buffer;threedee.Scene.prototype.globalAmbient=threedee.Color.DARK_GRAY;threedee.Scene.prototype.lastUpdate=0;threedee.Scene.prototype.setRootNode=function(rootNode){this.rootNode=rootNode};
threedee.Scene.prototype.getRootNode=function(){return this.rootNode};
threedee.Scene.prototype.setGlobalAmbient=function(globalAmbient){this.globalAmbient=globalAmbient};
threedee.Scene.prototype.getGlobalAmbient=function(){return this.globalAmbient};
threedee.Scene.prototype.update=function(delta){var now;if(!this.rootNode)return;if(delta===undefined){now=(new Date).getTime();delta=now-this.lastUpdate;this.lastUpdate=now;if(delta>1E4)return}this.rootNode.update(delta)};
threedee.Scene.prototype.render=function(g,width,height,camera,renderOptions){var rootTransform;if(!this.rootNode)return;rootTransform=this.rootNode.getTransform();if(camera)rootTransform=camera.getTransform().copy().invert().transform(rootTransform);this.buffer.prepare(width,height);this.buffer.setGlobalAmbient(this.globalAmbient);if(renderOptions)this.buffer.setRenderOptions(renderOptions);this.rootNode.render(this.buffer,rootTransform);this.buffer.render(g)};
threedee.Scene.prototype.getDebugInfo=function(){return this.buffer.getDebugInfo()};threedee.PolygonBuffer=function(){this.vertices=[];this.polygons=[];this.lights=[];this.fpsCounter=new threedee.FpsCounter;this.renderOptions=new threedee.RenderOptions};
threedee.PolygonBuffer.V=new threedee.Vector;threedee.PolygonBuffer.prototype.renderOptions;threedee.PolygonBuffer.prototype.debugInfo="";threedee.PolygonBuffer.prototype.vertices;threedee.PolygonBuffer.prototype.polygons;threedee.PolygonBuffer.prototype.lights;threedee.PolygonBuffer.prototype.width=0;threedee.PolygonBuffer.prototype.height=0;threedee.PolygonBuffer.prototype.factor=1;threedee.PolygonBuffer.prototype.frustum;threedee.PolygonBuffer.prototype.fpsCounter;
threedee.PolygonBuffer.prototype.globalAmbient=threedee.Color.DARK_GRAY;threedee.PolygonBuffer.prototype.setGlobalAmbient=function(globalAmbient){this.globalAmbient=globalAmbient};
threedee.PolygonBuffer.prototype.setRenderOptions=function(renderOptions){this.renderOptions=renderOptions};
threedee.PolygonBuffer.prototype.prepare=function(width,height){var eyeDistance,screenInMeters,screenInPixels,dpm;this.vertices=[];this.polygons=[];this.lights=[];if(width!=this.width||height!=this.height){eyeDistance=0.5;screenInMeters=0.38;screenInPixels=Math.min(width,height);dpm=screenInPixels/screenInMeters;this.factor=eyeDistance*dpm;this.frustum=new threedee.Frustum(width,height,this.factor);this.width=width;this.height=height}};
threedee.PolygonBuffer.prototype.addModel=function(model,transform){var backfaceCulling,i,max,polygon;backfaceCulling=this.renderOptions.backfaceCulling;model.transform(transform);for(i=0,max=model.countPolygons();i<max;i++){polygon=model.getPolygon(i);polygon.init();if(backfaceCulling&&polygon.isBackface())continue;if(this.frustum.clipPolygon(polygon))continue;polygon.updateAverageZ();this.polygons.push(polygon)}};
threedee.PolygonBuffer.prototype.addLight=function(light,transform){this.lights.push(new threedee.TransformedLight(light,transform))};
threedee.PolygonBuffer.prototype.render=function(g){var x,y,factor,i,max,vertexCount,polygon,v,vertex,dx,dy,dz,displayNormals,lighting,solid,normal,center,normalEnd,cx,cy,cx2,cy2,sortZ,debugInfo,fpsInfo,polygonCounter,vertexCounter;displayNormals=this.renderOptions.displayNormals;sortZ=this.renderOptions.sortZ;lighting=this.renderOptions.lighting;solid=this.renderOptions.solid;debugInfo=this.renderOptions.debugInfo;fpsInfo=this.renderOptions.fpsInfo;polygonCounter=0;vertexCounter=0;if(sortZ)this.polygons.sort(threedee.RenderPolygon.compare);
g.save();g.translate(this.width/2,this.height/2);factor=this.factor;g.strokeStyle="#fff";for(i=0,max=this.polygons.length;i<max;i++){polygon=this.polygons[i];vertexCount=polygon.countVertices();g.beginPath();for(v=0;v<vertexCount;v++){vertex=polygon.getVertex(v);dx=vertex.x;dy=vertex.y;dz=vertex.z;x=dx*factor/dz;y=-dy*factor/dz;if(v)g.lineTo(x,y);else g.moveTo(x,y)}g.closePath();if(solid){if(lighting)this.applyPolygonColor(polygon,g);else g.fillStyle=polygon.getMaterial().getDiffuse().toCSS();g.fill();
if(this.renderOptions.outline){g.strokeStyle=this.renderOptions.outlineColor||g.fillStyle;g.stroke()}}else g.stroke();if(displayNormals){normal=polygon.getNormal();g.save();g.strokeStyle=threedee.Color.YELLOW.toCSS();center=polygon.getCenter();normalEnd=normal.add(center);cx=center.x*factor/center.z;cy=-center.y*factor/center.z;cx2=normalEnd.x*factor/normalEnd.z;cy2=-normalEnd.y*factor/normalEnd.z;g.beginPath();g.moveTo(cx,cy);g.lineTo(cx2,cy2);g.stroke();g.restore()}if(debugInfo){polygonCounter++;
vertexCounter+=vertexCount}}if(fpsInfo||debugInfo){if(fpsInfo){this.fpsCounter.frame();this.debugInfo="Frames/s: "+this.fpsCounter.getFps();if(debugInfo)this.debugInfo+="\n\n"}else this.debugInfo="";if(debugInfo)this.debugInfo+="Vertices: "+vertexCounter+"\nPolygons: "+polygonCounter+"\n\nNew objects for this frame:"+"\n  Vector: "+threedee.Vector.count()+"\n  Matrix: "+threedee.Matrix.count()+"\n  Color: "+threedee.Color.count()+"\n  Material: "+threedee.Material.count()+"\n  Polygon: "+threedee.Polygon.count()}g.restore()};
threedee.PolygonBuffer.prototype.applyPolygonColor=function(polygon,g){var material,position,normal,globalAmbient,ambient,diffuse,emissive,result,addedDiffuse,i,j,max,transLight,light,lightColor,lightPosition,L,diffuseLight;material=polygon.getMaterial();position=polygon.getCenter();normal=polygon.getNormal();globalAmbient=this.globalAmbient.getComponents();ambient=material.getAmbient().getComponents();diffuse=material.getDiffuse().getComponents();emissive=material.getEmissive().getComponents();result=
new Array(3);addedDiffuse=[];addedDiffuse[0]=0;addedDiffuse[1]=0;addedDiffuse[2]=0;for(i=0;i<3;i++){ambient[i]=ambient[i]*globalAmbient[i];for(j=0,max=this.lights.length;j<max;j++){transLight=this.lights[j];light=transLight.getLight();if(light instanceof threedee.PointLight){lightColor=light.getColor().getComponent(i);lightPosition=transLight.getPosition().copy(threedee.PolygonBuffer.V);L=lightPosition.sub(position).toUnit();diffuseLight=Math.max(normal.dot(L),0);addedDiffuse[i]+=diffuse[i]*lightColor*
diffuseLight}}result[i]=Math.min(ambient[i]+emissive[i]+addedDiffuse[i],1)}g.fillStyle="rgb("+parseInt(result[0]*255,10)+","+parseInt(result[1]*255,10)+","+parseInt(result[2]*255,10)+")"};
threedee.PolygonBuffer.prototype.getDebugInfo=function(){return this.debugInfo};threedee.RenderOptions=function(){};
threedee.RenderOptions.prototype.displayNormals=false;threedee.RenderOptions.prototype.lighting=true;threedee.RenderOptions.prototype.solid=true;threedee.RenderOptions.prototype.backfaceCulling=true;threedee.RenderOptions.prototype.sortZ=true;threedee.RenderOptions.prototype.fpsInfo=false;threedee.RenderOptions.prototype.debugInfo=false;threedee.RenderOptions.prototype.outline=true;threedee.RenderOptions.prototype.outlineColor=null;