<?php require("../../../target/demo/resolver.php"); ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>ThreeDee Test: Freighter</title>
    <? $resolver->includeScript("threedee/rendering/RenderOptions.js") ?>
    <? $resolver->includeScript("threedee/model/Model.js") ?>
    <? $resolver->includeScript("threedee/updater/KeyboardUpdater.js") ?>
    <? $resolver->includeScript("threedee/light/PointLight.js") ?>
    <? $resolver->includeScript("threedee/SceneNode.js") ?>
    <? $resolver->includeScript("threedee/ModelNode.js") ?>
    <? $resolver->includeScript("threedee/LightNode.js") ?>
    <? $resolver->includeScript("threedee/CameraNode.js") ?>
    <? $resolver->includeScript("threedee/Scene.js") ?>
    <script src="models/freighter.js" type="text/javascript"></script>
    <script type="text/javascript">
    /* <![CDATA[ */

    // Setup render options
    var renderOptions = new threedee.RenderOptions();
    renderOptions.fpsInfo = true;
    renderOptions.debugInfo = true;

    // Creates the root node
    var rootNode = new threedee.SceneNode();

    // Create the freighter node and add it to the root node
    var freighter = threedee.Model.fromJSON(freighterModel);
    var freighterNode = new threedee.ModelNode(freighter);
    rootNode.appendChild(freighterNode);

    // Creates the camera node
    var camera = new threedee.CameraNode();
    rootNode.appendChild(camera);

    // Position the camera behind and a little bit above the spaceship
    camera.getTransform().translateY(5).translateZ(-40);
    
    // Create a light node and attach it to the camera node.
    var light = new threedee.PointLight();
    var lightNode = new threedee.LightNode(light);    
    camera.appendChild(lightNode);

    // Add keyboard updater to the freighter so user can steer it
    var keyboardUpdater = new threedee.KeyboardUpdater();
    freighterNode.addUpdater(keyboardUpdater);

    // Create the scene
    var scene = new threedee.Scene();    
    scene.setRootNode(rootNode);
    
    // The canvas and graphics context
    var canvas, ctx;

    function init()
    {
        canvas = document.getElementById("canvas");
        ctx = canvas.getContext("2d");
        setInterval(render, 20);
    }

    function render()
    {
        var width, height;
        
        width = canvas.width;
        height = canvas.height;

        ctx.clearRect(0, 0, width, height);

        scene.update(); 
        scene.render(ctx, width, height, renderOptions, camera);

        document.getElementById("debugInfo").innerHTML = scene.getDebugInfo();
    }

    function steerCamera()
    {
        this.camera.removeUpdater(keyboardUpdater);
        this.freighterNode.removeUpdater(keyboardUpdater);
        this.camera.addUpdater(keyboardUpdater);
    }
    
    function steerFreighter()
    {
        this.camera.removeUpdater(keyboardUpdater);
        this.freighterNode.removeUpdater(keyboardUpdater);
        this.freighterNode.addUpdater(keyboardUpdater);
    }
    
    /* ]]> */
    </script>
  </head>
  <body onload="init()">
    <h1>ThreeDee test: Freighter</h1>
    <p>
      Sshows a freighter which you can steer by using the cursor keys
      and the keys W, A, S, D, Q, E, R, and F. With the buttons below you can
      switch between steering the freighter and steering the camera. The
      light is connected to the camera so when you move the camera then you
      automatically also move the light.
    </p>
    <div style="position:relative">
      <canvas id="canvas" width="480" height="320" style="background:black"></canvas>
      <pre id="debugInfo" style="position:absolute;left:5px;top:5px;font-size:12px;color:#0f0;margin:0"></pre>
    </div>
    <div>
      <br />
      <button onclick="steerCamera()">Steer Camera</button>
      <button onclick="steerFreighter()">Steer Freighter</button>
    </div>
  </body>
</html>
