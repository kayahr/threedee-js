<?php require("../../../target/demo/resolver.php"); ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>ThreeDee Test: Cube</title>
    <? $resolver->includeScript("threedee/rendering/RenderOptions.js") ?>
    <? $resolver->includeScript("threedee/updater/KeyboardUpdater.js") ?>
    <? $resolver->includeScript("threedee/updater/PitchUpdater.js") ?>
    <? $resolver->includeScript("threedee/updater/YawUpdater.js") ?>
    <? $resolver->includeScript("threedee/updater/RollUpdater.js") ?>
    <? $resolver->includeScript("threedee/light/PointLight.js") ?>
    <? $resolver->includeScript("threedee/SceneNode.js") ?>
    <? $resolver->includeScript("threedee/LightNode.js") ?>
    <? $resolver->includeScript("threedee/CameraNode.js") ?>
    <? $resolver->includeScript("threedee/model/Cube.js") ?>
    <? $resolver->includeScript("threedee/ModelNode.js") ?>
    <? $resolver->includeScript("threedee/ModelNode.js") ?>
    <? $resolver->includeScript("threedee/Scene.js") ?>
    <script type="text/javascript">
    /* <![CDATA[ */

    var renderOptions = new threedee.RenderOptions();
    renderOptions.fpsInfo = true;
    renderOptions.debugInfo = true;

    // Creates the root node
    var rootNode = new threedee.SceneNode();

    // Create a simple cube and add it to the root node
    var cube = new threedee.Cube(10);
    var cubeNode = new threedee.ModelNode(cube);
    rootNode.appendChild(cubeNode);

    // Creates the camera node
    var camera = new threedee.CameraNode();
    rootNode.appendChild(camera);

    // Position the camera behind and a little bit above the spaceship
    camera.getTransform().translateZ(-100);
    
    // Add updaters to cube so it rotates nicely around all axes
    var pitchUpdater = new threedee.PitchUpdater(90 * Math.PI / 180);
    var yawUpdater = new threedee.YawUpdater(45 * Math.PI / 180);
    var rollUpdater = new threedee.RollUpdater(22.5 * Math.PI / 180);
    cubeNode.addUpdater(pitchUpdater);
    cubeNode.addUpdater(yawUpdater);
    cubeNode.addUpdater(rollUpdater);

    // Create a light node and add it to the root node
    var light = new threedee.PointLight();
    var lightNode = new threedee.LightNode(light);
    rootNode.appendChild(lightNode);

    // Place the light somwhere in the upper right corner of the scene
    lightNode.getTransform().translateZ(-100).translateX(25).translateY(25);

    // Create the scene
    var scene = new threedee.Scene();    
    scene.setRootNode(rootNode);
    
    // The canvas and graphics context
    var canvas, ctx;

    function init()
    {
        canvas = document.getElementById("canvas");
        ctx = canvas.getContext("2d");
        setInterval(render, 20);
    }

    function render()
    {
        var width, height;
        
        width = canvas.width;
        height = canvas.height;

        ctx.clearRect(0, 0, width, height);

        scene.update(); 
        scene.render(ctx, width, height, renderOptions, camera);

        document.getElementById("debugInfo").innerHTML = scene.getDebugInfo();
    }
    
    /* ]]> */
    </script>
  </head>
  <body onload="init()">
    <h1>ThreeDee demo: Cube</h1>
    <p>
      This demo shows a simple rotating cube. 
    </p>
    <hr />
    <div style="position:relative">
      <canvas id="canvas" width="480" height="320" style="background:black"></canvas>
      <pre id="debugInfo" style="position:absolute;left:5px;top:5px;font-size:12px;color:#0f0;margin:0"></pre>
    </div>
    <hr />
    <p>
      Copyright (C) 2009 Klaus Reimer
    </p>
  </body>
</html>
