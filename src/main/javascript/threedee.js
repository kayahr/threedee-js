/**
 * Copyright (C) 2009-2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information.  
 */
   
/** 
 * @license
 * ThreeDee - JavaScript 3D scene graph engine
 * http://kayahr.github.com/threedee
 * 
 * Copyright (C) 2009-2011 Klaus Reimer <k@ailis.de>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 * 
 */

/**
 * The threedee namespace.
 * @type {Object}
 */
var threedee = {};

/**
 * Derives subClass from superClass.
 * 
 * @param {Function} subClass
 *            The sub class
 * @param {Function} superClass
 *            The super class
 */
threedee.inherit = function(subClass, superClass)
{
    var tmp = superClass.prototype;
    superClass = new Function();
    superClass.prototype = tmp;
    subClass.prototype = new superClass();
    subClass.prototype.constructor = subClass;
};