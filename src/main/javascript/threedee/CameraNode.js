/**
 * Copyright (C) 2009-2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information
 * 
 * @require threedee.js
 * @require threedee/SceneNode.js
 * @use threedee/Matrix.js
 */

/**
 * Constructs a new camera node.
 * 
 * @constructor
 * @extends {threedee.SceneNode}
 * @class 
 * A camera node.
 */
threedee.CameraNode = function()
{
    threedee.SceneNode.call(this);
};
threedee.inherit(threedee.CameraNode, threedee.SceneNode);

/**
 * Transforms the node so it looks at the specified point.
 * 
 * @param {threedee.Vector} eye
 *            The position of the eye
 * @param {threedee.Vector} center
 *            The point the eye looks at
 * @param {threedee.Vector} up
 *            The up vector of the eye
 *            The transformation matrix
 */
threedee.CameraNode.prototype.lookAt = function(eye, center, up)
{
    var forwardx, forwardy, forwardz, invMag, upx, upy, upz,
        sidex, sidey, sidez, mat;

    forwardx = eye.x - center.x;
    forwardy = eye.y - center.y;
    forwardz = eye.z - center.z;

    invMag =
        1.0 / Math.sqrt(forwardx * forwardx + forwardy * forwardy
            + forwardz * forwardz);
    forwardx = forwardx * invMag;
    forwardy = forwardy * invMag;
    forwardz = forwardz * invMag;

    invMag = 1.0 / Math.sqrt(up.x * up.x + up.y * up.y + up.z * up.z);
    upx = up.x * invMag;
    upy = up.y * invMag;
    upz = up.z * invMag;

    // side = Up cross forward
    sidex = upy * forwardz - forwardy * upz;
    sidey = upz * forwardx - upx * forwardz;
    sidez = upx * forwardy - upy * forwardx;

    invMag =
        1.0 / Math.sqrt(sidex * sidex + sidey * sidey + sidez * sidez);
    sidex *= invMag;
    sidey *= invMag;
    sidez *= invMag;

    // recompute up = forward cross side

    upx = forwardy * sidez - sidey * forwardz;
    upy = forwardz * sidex - forwardx * sidez;
    upz = forwardx * sidey - forwardy * sidex;

    mat = [];
    
    // transpose because we calculated the inverse of what we want
    mat[0] = sidex;
    mat[1] = sidey;
    mat[2] = sidez;

    mat[4] = upx;
    mat[5] = upy;
    mat[6] = upz;

    mat[8] = forwardx;
    mat[9] = forwardy;
    mat[10] = forwardz;

    mat[3] = -eye.x * mat[0] + -eye.y * mat[1] + -eye.z * mat[2];
    mat[7] = -eye.x * mat[4] + -eye.y * mat[5] + -eye.z * mat[6];
    mat[11] = -eye.x * mat[8] + -eye.y * mat[9] + -eye.z * mat[10];

    mat[12] = mat[13] = mat[14] = 0;
    mat[15] = 1;
   
    var m = new threedee.Matrix().set(
        mat[0], mat[1], mat[2], mat[3],
        mat[4], mat[5], mat[6], mat[7],
        mat[8], mat[9], mat[10], mat[11],
        mat[12], mat[13], mat[14], mat[15]);
    this.setTransform(m);
};
