/**
 * Copyright (C) 2009-2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information
 * 
 * @require threedee.js
 */

/**
 * Constructs a new color.
 * 
 * @param {number} red
 *            The red component (0.0 - 1.0)
 * @param {number} green
 *            The green component (0.0 - 1.0)
 * @param {number} blue
 *            The blue component (0.0 - 1.0)
 *            
 * @constructor
 * @class A color
 */
threedee.Color = function(red, green, blue)
{
    if (red) this.red = red;
    if (green) this.green = green;
    if (blue) this.blue = blue;
    this.css = "rgb(" + this.red * 255 + "," + this.green * 255 + "," +
        this.blue * 255 + ")";
    threedee.Color.counter++;
};

/** 
 * Instance counter. 
 * @private 
 * @type {number} 
 */
threedee.Color.counter = 0;

/** 
 * The red component. 
 * @private 
 * @type {number} 
 */
threedee.Color.prototype.red = 0;

/** 
 * The red component. 
 * @private 
 * @type {number} 
 */
threedee.Color.prototype.green = 0;

/** 
 * The red component. 
 * @private 
 * @type {number} 
 */
threedee.Color.prototype.blue = 0;

/** 
 * The CSS representation of the color. 
 * @private 
 * @type {string} 
 */
threedee.Color.prototype.css = "rgb(0,0,0)";

/** 
 * Black color. 
 * @final 
 * @type {!threedee.Color} 
 */
threedee.Color.BLACK = new threedee.Color(0, 0, 0);

/** 
 * Red color.
 * @final 
 * @type {!threedee.Color} 
 */
threedee.Color.RED = new threedee.Color(1, 0, 0);

/** 
 * Green color. 
 * @final 
 * @type {!threedee.Color} 
 */
threedee.Color.GREEN = new threedee.Color(0, 1, 0);

/** 
 * Blue color. 
 * @final 
 * @type {!threedee.Color} 
 */
threedee.Color.BLUE = new threedee.Color(0, 0, 1);

/** 
 * Dark gray color. 
 * @final 
 * @type {!threedee.Color} 
 */
threedee.Color.DARK_GRAY = new threedee.Color(0.25, 0.25, 0.25);

/** 
 * Yellow color. 
 * @final 
 * @type {!threedee.Color} 
 */
threedee.Color.YELLOW = new threedee.Color(1, 1, 0);

/** 
 * White color. 
 * @final
 * @type {!threedee.Color} 
 */
threedee.Color.WHITE = new threedee.Color(1, 1, 1);

/**
 * Returns and resets the current instance counter.
 * 
 * @return {number} 
 *            The number of created instances since the last call.
 */
threedee.Color.count = function()
{
    var value = threedee.Color.counter;
    threedee.Color.counter = 0;
    return value;
};

/**
 * Returns the red component.
 * 
 * @return {number} 
 *            The red component.
 */
threedee.Color.prototype.getRed = function()
{
    return this.red;
};

/**
 * Returns the green component (0.0-1.0).
 * 
 * @return {number} 
 *            The green component.
 */
threedee.Color.prototype.getGreen = function()
{
    return this.green;
};

/**
 * Returns the blue component (0.0-1.0).
 * 
 * @return {number} 
 *            The blue component.
 */
threedee.Color.prototype.getBlue = function()
{
    return this.blue;
};

/**
 * Returns the CSS representation of the color (0.0-1.0).
 * 
 * @return {string} 
 *            The CSS representation of the color.
 */
threedee.Color.prototype.toCSS = function()
{
    return this.css;
};

/**
 * Returns the three color components as an array. The entries are in the
 * range from 0.0 to 1.0.
 *
 * @return {!Array.<number>} 
 *            The three color components as an array
 */
threedee.Color.prototype.getComponents = function()
{
    return [ this.red, this.green, this.blue ];
};

/**
 * Returns a color component. 0=red, 1=green, 2=blue.
 *
 * @param {number} component
 *            The component index
 * @return {number} The color component (0-1)
 */
threedee.Color.prototype.getComponent = function(component)
{
    return (component ? (component == 1 ? this.green : this.blue) : this.red);
};

/**
 * Converts the color into a JSON object with keys 'r', 'g' and 'b' and
 * returns it.
 * 
 * @return {Object} The color as a JSON object
 */
threedee.Color.prototype.toJSON = function()
{
    return { "r": this.red, "g": this.green, "b": this.blue };
};

/**
 * Creates a new color instance with the color components read from the
 * specified JSON object (with keys 'r', 'g' and 'b'). Returns null if
 * data was empty
 * 
 * @param {Object} data
 *            The color as JSON object
 * @return {threedee.Color} The color object or null if data was empty
 */
threedee.Color.fromJSON = function(data)
{
    if (!data) return null;
    return new threedee.Color(+data["r"], +data["g"], +data["b"]);
};
