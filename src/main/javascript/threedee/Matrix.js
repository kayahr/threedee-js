/**
 * Copyright (C) 2009-2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information
 * 
 * @require threedee.js
 */

/**
 * @constructor
 * Constructs a new matrix initialized as an identity matrix.
 *            
 * @class
 * A matrix with 4x4 entries.
 */
threedee.Matrix = function()
{
    threedee.Matrix.counter++;
};

/** 
 * Instance counter. 
 * @private 
 * @type {number} 
 */
threedee.Matrix.counter = 0;

/** 
 * A temporary matrix for internal operations. 
 * @private 
 * @type {!threedee.Matrix} 
 */
threedee.Matrix.TMP = new threedee.Matrix();

/** 
 * The matrix entry 0;0. 
 * @type {number} 
 */
threedee.Matrix.prototype.m00 = 1;

/** 
 * The matrix entry 0;1.
 * @type {number} 
 */
threedee.Matrix.prototype.m01 = 0;

/** 
 * The matrix entry 0;2. 
 * @type {number} 
 */
threedee.Matrix.prototype.m02 = 0;

/** 
 * The matrix entry 0;3. 
 * @type {number} 
 */
threedee.Matrix.prototype.m03 = 0;

/** 
 * The matrix entry 1;0. 
 * @type {number} 
 */
threedee.Matrix.prototype.m10 = 0;

/** 
 * The matrix entry 1;1. 
 * @type {number} 
 */
threedee.Matrix.prototype.m11 = 1;

/** 
 * The matrix entry 1;2. 
 * @type {number} 
 */
threedee.Matrix.prototype.m12 = 0;

/** 
 * The matrix entry 1;3. 
 * @type {number} 
 */
threedee.Matrix.prototype.m13 = 0;

/** 
 * The matrix entry 2;0.
 * @type {number} 
 */
threedee.Matrix.prototype.m20 = 0;

/** 
 * The matrix entry 2;1. 
 * @type {number} 
 */
threedee.Matrix.prototype.m21 = 0;

/** 
 * The matrix entry 2;2. 
 * @type {number} *
 */
threedee.Matrix.prototype.m22 = 1;

/** 
 * The matrix entry 2;3. 
 * @type {number} 
 */
threedee.Matrix.prototype.m23 = 0;

/** 
 * The matrix entry 3;0. 
 * @type {number} 
 */
threedee.Matrix.prototype.m30 = 0;

/** 
 * The matrix entry 3;1. 
 * @type {number} 
 */
threedee.Matrix.prototype.m31 = 0;

/** 
 * The matrix entry 3;2. 
 * @type {number} 
 */
threedee.Matrix.prototype.m32 = 0;

/** 
 * The matrix entry 3;3. 
 * @type {number} 
 */
threedee.Matrix.prototype.m33 = 1;

/**
 * Returns and resets the current instance counter.
 * 
 * @return {number} 
 *             The number of created instances since the last call.
 */
threedee.Matrix.count = function()
{
    var value = threedee.Matrix.counter;
    threedee.Matrix.counter = 0;
    return value;
};

/**
 * Returns a copy of this matrix.
 * 
 * @return {!threedee.Matrix} 
 *            A copy of this matrix
 */
threedee.Matrix.prototype.copy = function()
{
    return new threedee.Matrix().set(
        this.m00, this.m01, this.m02, this.m03,
        this.m10, this.m11, this.m12, this.m13,
        this.m20, this.m21, this.m22, this.m23,
        this.m30, this.m31, this.m32, this.m33);
};

/**
 * Sets the matrix entries.
 * 
 * @param {number} m00
 *            The matrix entry 0;0
 * @param {number} m01
 *            The matrix entry 0;1
 * @param {number} m02
 *            The matrix entry 0;2
 * @param {number} m03
 *            The matrix entry 0;3
 * @param {number} m10
 *            The matrix entry 1;0
 * @param {number} m11
 *            The matrix entry 1;1
 * @param {number} m12
 *            The matrix entry 1;2
 * @param {number} m13
 *            The matrix entry 1;3
 * @param {number} m20
 *            The matrix entry 2;0
 * @param {number} m21
 *            The matrix entry 2;1
 * @param {number} m22
 *            The matrix entry 2;2
 * @param {number} m23
 *            The matrix entry 2;3
 * @param {number} m30
 *            The matrix entry 3;0
 * @param {number} m31
 *            The matrix entry 3;1
 * @param {number} m32
 *            The matrix entry 3;2
 * @param {number} m33
 *            The matrix entry 3;3
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.set = function(m00, m01, m02, m03,
    m10, m11, m12, m13, m20, m21, m22, m23, m30, m31, m32, m33)
{
    this.m00 = m00;
    this.m01 = m01;
    this.m02 = m02;
    this.m03 = m03;
    this.m10 = m10;
    this.m11 = m11;
    this.m12 = m12;
    this.m13 = m13;
    this.m20 = m20;
    this.m21 = m21;
    this.m22 = m22;
    this.m23 = m23;
    this.m30 = m30;
    this.m31 = m31;
    this.m32 = m32;
    this.m33 = m33;
    return this;
};

/**
 * Sets the entries of this matrix to an identity matrix.
 * 
 * @return {!threedee.Matrix}
 *             The matrix
 */
threedee.Matrix.prototype.setIdentity = function()
{
    return this.set(
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1);
};

/**
 * Sets the entries of this matrix to an X rotation matrix.
 * 
 * @param {number} angle
 *            The rotation angle in clock-wise RAD
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.setRotateX = function(angle)
{
    var s, c;
    
    s = Math.sin(angle);
    c = Math.cos(angle);
    return this.set(
        1, 0, 0, 0,
        0, c, -s, 0,
        0, s, c, 0,
        0, 0, 0, 1);
};

/**
 * Sets the entries of this matrix to an Y rotation matrix.
 * 
 * @param {number} angle
 *            The rotation angle in anti-clock-wise RAD
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.setRotateY = function(angle)
{
    var s, c;
    
    s = Math.sin(angle);
    c = Math.cos(angle);
    return this.set(
        c, 0, s, 0,
        0, 1, 0, 0,
        -s, 0, c, 0,
        0, 0, 0, 1);
};

/**
 * Sets the entries of this matrix to an Z rotation matrix.
 * 
 * @param {number} angle
 *            The rotation angle in anti-clock-wise RAD
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.setRotateZ = function(angle)
{
    var s, c;
    
    s = Math.sin(angle);
    c = Math.cos(angle);
    return this.set(
        c, -s, 0, 0,
        s, c, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1);
};

/**
 * Sets the entries of this matrix to a scaling matrix.
 * 
 * @param {number} fx
 *            The X scale factor
 * @param {number} fy
 *            The Y scale factor. Optional. Defaults to fx.
 * @param {number} fz
 *            The Z scale factor. Optional. Defaults to fx.
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.setScale = function(fx, fy, fz)
{
    return this.set(
        fx, 0, 0, 0,
        0, fy === undefined ? fx : fy, 0, 0,
        0, 0, fz === undefined ? fx : fz, 0,
        0, 0, 0, 1);
};

/**
 * Sets the entries of this matrix to a X scaling matrix.
 * 
 * @param {number} f
 *            The scale factor
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.setScaleX = function(f)
{
    return this.set(
        f, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1);
};

/**
 * Sets the entries of this matrix to a Y scaling matrix.
 * 
 * @param {number} f
 *            The scale factor
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.setScaleY = function(f)
{
    return this.set(
        1, 0, 0, 0,
        0, f, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1);
};

/**
 * Sets the entries of this matrix to a Z scaling matrix.
 * 
 * @param {number} f
 *            The scale factor
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.setScaleZ = function(f)
{
    return this.set(
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, f, 0,
        0, 0, 0, 1);
};

/**
 * Sets the entries of this matrix to a translation matrix.
 * 
 * @param {number} dx
 *            The X delta
 * @param {number} dy
 *            The Y delta.
 * @param {number} dz
 *            The Z delta.
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.setTranslate = function(dx, dy, dz)
{
    return this.set(
        1, 0, 0, dx,
        0, 1, 0, dy,
        0, 0, 1, dz,
        0, 0, 0, 1);
};

/**
 * Sets the entries of this matrix to a X translation matrix.
 * 
 * @param {number} d
 *            The delta
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.setTranslateX = function(d)
{
    return this.set(
        1, 0, 0, d,
        0, 1, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1);
};

/**
 * Sets the entries of this matrix to a Y translation matrix.
 * 
 * @param {number} d
 *            The delta
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.setTranslateY = function(d)
{
    return this.set(
        1, 0, 0, 0,
        0, 1, 0, d,
        0, 0, 1, 0,
        0, 0, 0, 1);
};

/**
 * Sets the entries of this matrix to a Z translation matrix.
 * 
 * @param {number} d
 *            The delta
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.setTranslateZ = function(d)
{
    return this.set(
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, d,
        0, 0, 0, 1);
};

/**
 * Multiplies this matrix with the specified matrix. The result is written
 * to this matrix.
 * 
 * @param {!threedee.Matrix} m
 *            The other matrix
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.transform = function(m)
{
    return this.set(this.m00 * m.m00 + this.m01 * m.m10 + this.m02
        * m.m20 + this.m03 * m.m30, this.m00 * m.m01 + this.m01 * m.m11
        + this.m02 * m.m21 + this.m03 * m.m31, this.m00 * m.m02 + this.m01
        * m.m12 + this.m02 * m.m22 + this.m03 * m.m32, this.m00 * m.m03
        + this.m01 * m.m13 + this.m02 * m.m23 + this.m03 * m.m33,

    this.m10 * m.m00 + this.m11 * m.m10 + this.m12 * m.m20 + this.m13
        * m.m30, this.m10 * m.m01 + this.m11 * m.m11 + this.m12 * m.m21
        + this.m13 * m.m31, this.m10 * m.m02 + this.m11 * m.m12 + this.m12
        * m.m22 + this.m13 * m.m32, this.m10 * m.m03 + this.m11 * m.m13
        + this.m12 * m.m23 + this.m13 * m.m33,

    this.m20 * m.m00 + this.m21 * m.m10 + this.m22 * m.m20 + this.m23
        * m.m30, this.m20 * m.m01 + this.m21 * m.m11 + this.m22 * m.m21
        + this.m23 * m.m31, this.m20 * m.m02 + this.m21 * m.m12 + this.m22
        * m.m22 + this.m23 * m.m32, this.m20 * m.m03 + this.m21 * m.m13
        + this.m22 * m.m23 + this.m23 * m.m33,

    this.m30 * m.m00 + this.m31 * m.m10 + this.m32 * m.m20 + this.m33
        * m.m30, this.m30 * m.m01 + this.m31 * m.m11 + this.m32 * m.m21
        + this.m33 * m.m31, this.m30 * m.m02 + this.m31 * m.m12 + this.m32
        * m.m22 + this.m33 * m.m32, this.m30 * m.m03 + this.m31 * m.m13
        + this.m32 * m.m23 + this.m33 * m.m33);
};

/**
 * Multiplies this matrix with the specified factor.
 * 
 * @param {number} f
 *            The factor
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.multiply = function(f)
{
    this.m00 *= f;
    this.m01 *= f;
    this.m02 *= f;
    this.m03 *= f;
    this.m10 *= f;
    this.m11 *= f;
    this.m12 *= f;
    this.m13 *= f;
    this.m20 *= f;
    this.m21 *= f;
    this.m22 *= f;
    this.m23 *= f;
    this.m30 *= f;
    this.m31 *= f;
    this.m32 *= f;
    this.m33 *= f;
    return this;
};

/**
 * Divides this matrix by the specified factor.
 * 
 * @param {number} f
 *            The factor
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.divide = function(f)
{
    this.m00 /= f;
    this.m01 /= f;
    this.m02 /= f;
    this.m03 /= f;
    this.m10 /= f;
    this.m11 /= f;
    this.m12 /= f;
    this.m13 /= f;
    this.m20 /= f;
    this.m21 /= f;
    this.m22 /= f;
    this.m23 /= f;
    this.m30 /= f;
    this.m31 /= f;
    this.m32 /= f;
    this.m33 /= f;
    return this;
};

/**
 * Translates this matrix by the specified deltas
 * 
 * @param {number} dx
 *            The X delta
 * @param {number} dy
 *            The Y delta
 * @param {number} dz
 *            The Z delta
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.translate = function(dx, dy, dz)
{
    return this.transform(threedee.Matrix.TMP.setTranslate(dx, dy, dz));
};

/**
 * X-Translates this matrix by the specified delta
 * 
 * @param {number} d
 *            The delta
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.translateX = function(d)
{
    return this.transform(threedee.Matrix.TMP.setTranslateX(d));
};

/**
 * Y-Translates this matrix by the specified delta
 * 
 * @param {number} d
 *            The delta
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.translateY = function(d)
{
    return this.transform(threedee.Matrix.TMP.setTranslateY(d));
};

/**
 * Z-Translates this matrix by the specified delta
 * 
 * @param {number} d
 *            The delta
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.translateZ = function(d)
{
    return this.transform(threedee.Matrix.TMP.setTranslateZ(d));
};

/**
 * Scales this matrix by the specified factors
 * 
 * @param {number} fx
 *            The X factor
 * @param {number} fy
 *            The Y factor. Optional. Defaults to fx.
 * @param {number} fz
 *            The Z factor. Optional. Defaults to fx.
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.scale = function(fx, fy, fz)
{
    return this.transform(threedee.Matrix.TMP.setScale(fx,
        fy === undefined ? fx : fy, fz === undefined ? fx : fz));
};

/**
 * X-Scales this matrix by the specified factor
 * 
 * @param {number} f
 *            The factor
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.scaleX = function(f)
{
    return this.transform(threedee.Matrix.TMP.setScaleX(f));
};

/**
 * Y-Scales this matrix by the specified factor
 * 
 * @param {number} f
 *            The factor
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.scaleY = function(f)
{
    return this.transform(threedee.Matrix.TMP.setScaleY(f));
};

/**
 * Z-Scales this matrix by the specified factor
 * 
 * @param {number} f
 *            The factor
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.scaleZ = function(f)
{
    return this.transform(threedee.Matrix.TMP.setScaleZ(f));
};

/**
 * X-Rotates this matrix by the specified angle
 * 
 * @param {number} r
 *            The angle in clock-wise RAD
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.rotateX = function(r)
{
    return this.transform(threedee.Matrix.TMP.setRotateX(r));
};

/**
 * Y-Rotates this matrix by the specified angle
 * 
 * @param {number} r
 *            The angle in clock-wise RAD
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.rotateY = function(r)
{
    return this.transform(threedee.Matrix.TMP.setRotateY(r));
};

/**
 * Z-Rotates this matrix by the specified angle
 * 
 * @param {number} r
 *            The angle in clock-wise RAD
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.rotateZ = function(r)
{
    return this.transform(threedee.Matrix.TMP.setRotateZ(r));
};

/**
 * Returns the determinant of the matrix.
 * 
 * @return {number} The determinant of the matrix
 */
threedee.Matrix.prototype.getDeterminant = function()
{
    return this.m03 * this.m12 * this.m21 * this.m30 - this.m02
         * this.m13 * this.m21 * this.m30 - this.m03 * this.m11
         * this.m22 * this.m30 + this.m01 * this.m13 * this.m22
         * this.m30 + this.m02 * this.m11 * this.m23 * this.m30
         - this.m01 * this.m12 * this.m23 * this.m30 - this.m03
         * this.m12 * this.m20 * this.m31 + this.m02 * this.m13
         * this.m20 * this.m31 + this.m03 * this.m10 * this.m22
         * this.m31 - this.m00 * this.m13 * this.m22 * this.m31
         - this.m02 * this.m10 * this.m23 * this.m31 + this.m00
         * this.m12 * this.m23 * this.m31 + this.m03 * this.m11
         * this.m20 * this.m32 - this.m01 * this.m13 * this.m20
         * this.m32 - this.m03 * this.m10 * this.m21 * this.m32
         + this.m00 * this.m13 * this.m21 * this.m32 + this.m01
         * this.m10 * this.m23 * this.m32 - this.m00 * this.m11
         * this.m23 * this.m32 - this.m02 * this.m11 * this.m20
         * this.m33 + this.m01 * this.m12 * this.m20 * this.m33
         + this.m02 * this.m10 * this.m21 * this.m33 - this.m00
         * this.m12 * this.m21 * this.m33 - this.m01 * this.m10
         * this.m22 * this.m33 + this.m00 * this.m11 * this.m22
         * this.m33;
};

/**
 * Inverts this matrix.
 * 
 * @return {!threedee.Matrix}
 *            This matrix
 */
threedee.Matrix.prototype.invert = function()
{
    return this.set(
        this.m12 * this.m23 * this.m31 - this.m13
            * this.m22 * this.m31 + this.m13 * this.m21 * this.m32
            - this.m11 * this.m23 * this.m32 - this.m12 * this.m21
            * this.m33 + this.m11 * this.m22 * this.m33,
    
        this.m03 * this.m22 * this.m31 - this.m02 * this.m23
            * this.m31 - this.m03 * this.m21 * this.m32 + this.m01
            * this.m23 * this.m32 + this.m02 * this.m21 * this.m33
            - this.m01 * this.m22 * this.m33,
    
        this.m02 * this.m13 * this.m31 - this.m03 * this.m12
            * this.m31 + this.m03 * this.m11 * this.m32 - this.m01
            * this.m13 * this.m32 - this.m02 * this.m11 * this.m33
            + this.m01 * this.m12 * this.m33,
    
        this.m03 * this.m12 * this.m21 - this.m02 * this.m13
            * this.m21 - this.m03 * this.m11 * this.m22 + this.m01
            * this.m13 * this.m22 + this.m02 * this.m11 * this.m23
            - this.m01 * this.m12 * this.m23,
    
        this.m13 * this.m22 * this.m30 - this.m12 * this.m23
            * this.m30 - this.m13 * this.m20 * this.m32 + this.m10
            * this.m23 * this.m32 + this.m12 * this.m20 * this.m33
            - this.m10 * this.m22 * this.m33,
    
        this.m02 * this.m23 * this.m30 - this.m03 * this.m22
            * this.m30 + this.m03 * this.m20 * this.m32 - this.m00
            * this.m23 * this.m32 - this.m02 * this.m20 * this.m33
            + this.m00 * this.m22 * this.m33,
    
        this.m03 * this.m12 * this.m30 - this.m02 * this.m13
            * this.m30 - this.m03 * this.m10 * this.m32 + this.m00
            * this.m13 * this.m32 + this.m02 * this.m10 * this.m33
            - this.m00 * this.m12 * this.m33,
    
        this.m02 * this.m13 * this.m20 - this.m03 * this.m12
            * this.m20 + this.m03 * this.m10 * this.m22 - this.m00
            * this.m13 * this.m22 - this.m02 * this.m10 * this.m23
            + this.m00 * this.m12 * this.m23,
    
        this.m11 * this.m23 * this.m30 - this.m13 * this.m21
            * this.m30 + this.m13 * this.m20 * this.m31 - this.m10
            * this.m23 * this.m31 - this.m11 * this.m20 * this.m33
            + this.m10 * this.m21 * this.m33,
    
        this.m03 * this.m21 * this.m30 - this.m01 * this.m23
            * this.m30 - this.m03 * this.m20 * this.m31 + this.m00
            * this.m23 * this.m31 + this.m01 * this.m20 * this.m33
            - this.m00 * this.m21 * this.m33,
    
        this.m01 * this.m13 * this.m30 - this.m03 * this.m11
            * this.m30 + this.m03 * this.m10 * this.m31 - this.m00
            * this.m13 * this.m31 - this.m01 * this.m10 * this.m33
            + this.m00 * this.m11 * this.m33,
    
        this.m03 * this.m11 * this.m20 - this.m01 * this.m13
            * this.m20 - this.m03 * this.m10 * this.m21 + this.m00
            * this.m13 * this.m21 + this.m01 * this.m10 * this.m23
            - this.m00 * this.m11 * this.m23,
    
        this.m12 * this.m21 * this.m30 - this.m11 * this.m22
            * this.m30 - this.m12 * this.m20 * this.m31 + this.m10
            * this.m22 * this.m31 + this.m11 * this.m20 * this.m32
            - this.m10 * this.m21 * this.m32,
    
        this.m01 * this.m22 * this.m30 - this.m02 * this.m21
            * this.m30 + this.m02 * this.m20 * this.m31 - this.m00
            * this.m22 * this.m31 - this.m01 * this.m20 * this.m32
            + this.m00 * this.m21 * this.m32,
    
        this.m02 * this.m11 * this.m30 - this.m01 * this.m12
            * this.m30 - this.m02 * this.m10 * this.m31 + this.m00
            * this.m12 * this.m31 + this.m01 * this.m10 * this.m32
            - this.m00 * this.m11 * this.m32,
    
        this.m01 * this.m12 * this.m20 - this.m02 * this.m11
            * this.m20 + this.m02 * this.m10 * this.m21 - this.m00
            * this.m12 * this.m21 - this.m01 * this.m10 * this.m22
            + this.m00 * this.m11 * this.m22).divide(this.getDeterminant());
};
