/**
 * Copyright (C) 2009-2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information
 * 
 * @require threedee.js
 */

/**
 * @constructor
 * Constructs a new empty vector.
 * 
 * @param {number=} x
 *            The X coordinate (Optional)
 * @param {number=} y
 *            The Y coordinate (Optional)
 * @param {number=} z
 *            The Z coordinate (Optional)
 * 
 * @class
 * A vector with three elements.
 */
threedee.Vector = function(x, y, z)
{
    if (x) this.x = x;
    if (y) this.y = y;
    if (z) this.z = z;
    threedee.Vector.counter++;
};

/** 
 * Instance counter. 
 * @private 
 * @type {number} 
 */
threedee.Vector.counter = 0;

/** 
 * The X coordinate. 
 * @type {number} 
 */
threedee.Vector.prototype.x = 0;

/** 
 * The Y coordinate. 
 * @type {number} 
 */
threedee.Vector.prototype.y = 0;

/** 
 * The Z coordinate. 
 * @type {number} 
 */
threedee.Vector.prototype.z = 0;

/**
 * Returns and resets the current instance counter.
 * 
 * @return {number} 
 *            The number of created instances since the last call
 */
threedee.Vector.count = function()
{
    var value = threedee.Vector.counter;
    threedee.Vector.counter = 0;
    return value;
};

/**
 * Returns a copy of this vector.
 * 
 * @param {?threedee.Vector=} v
 *            Optional target vector
 * @return {!threedee.Vector} A copy of this vector (or the target vector)
 */
threedee.Vector.prototype.copy = function(v)
{
    return (v ? v : new threedee.Vector()).set(this.x, this.y, this.z);
};

/**
 * Sets the vector coordinates.
 * 
 * @param {number} x
 *            The X coordinate
 * @param {number} y
 *            The Y coordinate
 * @param {number} z
 *            The Z coordinate
 * @return {!threedee.Vector}
 *            This vector
 */            
threedee.Vector.prototype.set = function(x, y, z)
{
    this.x = x;
    this.y = y;
    this.z = z;
    return this;
};

/**
 * Adds the coordinates of the specified vector to this one.
 * 
 * @param {!threedee.Vector} v
 *            The vector to add
 * @return {!threedee.Vector} This vector
 */
threedee.Vector.prototype.add = function(v)
{
    this.x += v.x;
    this.y += v.y;
    this.z += v.z;
    return this;
};

/**
 * Subtracts the coordinates of the specified vector from this one.
 * 
 * @param {!threedee.Vector} v
 *            The vector to subtract
 * @return {!threedee.Vector} This vector
 */
threedee.Vector.prototype.sub = function(v)
{
    this.x -= v.x;
    this.y -= v.y;
    this.z -= v.z;
    return this;
};

/**
 * Scales the vector with the specified factors.
 * 
 * @param {number} fx
 *            The X factor
 * @param {number} fy
 *            The Y factor (Optional. Defaults to fx)
 * @param {number} fz
 *            The Z factor (Optional. Defaults to fx)
 * @return {!threedee.Vector} This vector
 */
threedee.Vector.prototype.scale = function(fx, fy, fz)
{
    this.x *= fx;
    this.y *= fy === undefined ? fx : fy;
    this.z *= fy === undefined ? fx : fz;
    return this;
};

/**
 * Creates and returns the dot product of this vector and the specified one.
 * 
 * @param {!threedee.Vector} v
 *            The other vector
 * @return {number}
 *            The dot product
 */
threedee.Vector.prototype.dot = function(v)
{
    return this.x * v.x + this.y * v.y + this.z * v.z;   
};

/**
 * Creates the cross product of this vector and the specified one and stores
 * the result back into this vector.
 * 
 * @param {!threedee.Vector} v
 *            The other vector
 * @return {!threedee.Vector} This vector
 */
threedee.Vector.prototype.cross = function(v)
{
    var x, y, z;
    
    x = this.y * v.z - this.z * v.y;
    y = this.z * v.x - this.x * v.z;
    z = this.x * v.y - this.y * v.x;
    this.x = x;
    this.y = y;
    this.z = z;
    return this;
};

/**
 * Returns the length of the vector.
 * 
 * @return {number} The vector length
 */
threedee.Vector.prototype.length = function()
{
    return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
};

/**
 * Converts this vector into a unit vector with length 1.
 * 
 * @return {!threedee.Vector} This vector
 */
threedee.Vector.prototype.toUnit = function()
{
    var len;
    
    len = this.length();
    this.x /= len;
    this.y /= len;
    this.z /= len;
    return this;
};

/**
 * Returns the angle between this vector and the specified one.
 * 
 * @param {!threedee.Vector} v
 *            The other vector
 * @return {number} 
 *            The angle in clockwise RAD.
 */
threedee.Vector.prototype.getAngle = function(v)
{
    return Math.acos(this.copy().toUnit().dot(v.toUnit()));
};

/**
 * Transforms this vector with the specified matrix.
 * 
 * @param {!threedee.Matrix} m
 *            The matrix
 * @return {!threedee.Vector}
 *            This vector
 */
threedee.Vector.prototype.transform = function(m)
{
    return this.set(
        m.m00 * this.x + m.m01 * this.y + m.m02 * this.z + m.m03,
        m.m10 * this.x + m.m11 * this.y + m.m12 * this.z + m.m13,
        m.m20 * this.x + m.m21 * this.y + m.m22 * this.z + m.m23);
};

/**
 * Converts the vector into a JSON object with keys 'x', 'y' and 'z'.
 * 
 * @return {Object}
 *            The vector as a JSON object
 */
threedee.Vector.prototype.toJSON = function()
{
    return { "x": this.x, "y": this.y, "z": this.z };
};

/**
 * Creates a new vector instance with the data read from the
 * specified JSON object (with keys 'x', 'y' and 'z'). Returns null if data
 * was empty.
 * 
 * @param {Object} data
 *            The vector as JSON object
 * @return {threedee.Vector} The vector object or null if data was empty
 */
threedee.Vector.fromJSON = function(data)
{
    if (!data) return null;
    return new threedee.Vector(+data["x"], +data["y"], +data["z"]);
};
