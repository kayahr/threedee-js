/**
 * Copyright (C) 2009-2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information
 * 
 * @require threedee.js
 * @require threedee/Vector.js
 */

/**
 * Constructs a new render polygon.
 *
 * @param {!threedee.Model} model
 *             The original model this polygon is connected to.
 * @param {!threedee.Polygon} polygon
 *             The original polygon.
 * @param {!Array.<!threedee.Vector>} vertices
 *             The vertices array.
 *             
 * @constructor
 * @class A render polygon is a wrapper around a polygon for internal usage
 * while rendering. 
 */
threedee.RenderPolygon = function(model, polygon, vertices)
{
    var i, max;
    
    this.model = model;
    this.material = polygon.getMaterial();
    this.vertices = [];
    this.size = polygon.countVertices();
    this.origSize = this.size;
    for (i = 0, max = this.size; i < max; i++)
        this.vertices.push(vertices[polygon.getVertex(i)]);
    
    this.polygon = [];
    this.prevPolygon = [];
};

/** 
 * Speed-optimization vector cache. 
 * @private 
 * @type {!threedee.Vector} 
 */
threedee.RenderPolygon.V1 = new threedee.Vector();

/** 
 * Speed-optimization vector cache. 
 * @private 
 * @type {!threedee.Vector} 
 */
threedee.RenderPolygon.V2 = new threedee.Vector();

/** 
 * The array with the used vertices.
 * @private 
 * @type {!Array.<!threedee.Vector>} 
 */
threedee.RenderPolygon.prototype.vertices;

/** 
 * The current number of used vertices. 
 * @private 
 * @type {number} 
 */
threedee.RenderPolygon.prototype.size = 0;

/** 
 * The original number of vertices. 
 * @private 
 * @type {number} 
 */
threedee.RenderPolygon.prototype.origSize = 0;

/** 
 * The extra vectors added to the original ones.
 * @private 
 * @type {number} 
 */
threedee.RenderPolygon.prototype.extras = 0;

/** 
 * The vertex indices that forms the polygon. 
 * @private 
 * @type {!Array.<number>} 
 */
threedee.RenderPolygon.prototype.polygon;

/** 
 * The previous vertex indices that forms the polygon. 
 * @private 
 * @type {!Array.<number>} 
 */
threedee.RenderPolygon.prototype.prevPolygon;

/** 
 * The normal vector cache. 
 * @private 
 * @type {threedee.Vector} 
 */
threedee.RenderPolygon.prototype.normal = null;

/** 
 * The center vector cache. 
 * @private 
 * @type {threedee.Vector} 
 */
threedee.RenderPolygon.prototype.center = null;

/** 
 * The referenced vertices. 
 * @private 
 * @type {!Array.<!threedee.Vector>} 
 */
threedee.RenderPolygon.prototype.vertices;

/** 
 * The average Z position of the polygon. 
 * @private 
 * @type {number} 
 */
threedee.RenderPolygon.prototype.averageZ = 0;

/** 
 * The polygon material. 
 * @private 
 * @type {?threedee.Material} 
 */
threedee.RenderPolygon.prototype.material = null;

/** 
 * The model this polygon is connected to. 
 * @private 
 * @type {!threedee.Model} 
 */
threedee.RenderPolygon.prototype.model;

/**
 * Initializes the polygon for the next rendering. This simply resets the
 * vertex indices to the original order and resets the counter of extra
 * vertices.
 */
threedee.RenderPolygon.prototype.init = function()
{
    var i;
    
    this.size = this.origSize;
    for (i = this.size - 1; i >= 0; i--) this.polygon[i] = i;
    this.extras = 0;
};

/**
 * Clips this polygon against the specified view plane.
 * 
 * @param {threedee.Plane} plane
 *            The clipping plane
 * @return {boolean}
 *            True if polygon was clipped completely away, false if it is
 *            still visible (even if clipped)
 */
threedee.RenderPolygon.prototype.clip = function(plane)
{
    var max, j, i, a, b, c, tmp, planeNormal, planeDistance, distanceA,
        distanceB, s, va, vb;
    
    // Swap the polygon arrays
    tmp = this.prevPolygon;
    this.prevPolygon = this.polygon;
    this.polygon = tmp;
    
    max = this.size;    
    j = 0;
    planeNormal = plane.getNormal();
    planeDistance = plane.getDistance();
    for (i = 0; i < max; i++)
    {
        // Get the two vertex indices of the next line
        a = this.prevPolygon[i];
        b = this.prevPolygon[(i + 1) % max];

        // Get the corresponding vertices and calculate the distances
        va = this.vertices[a];
        vb = this.vertices[b];
        distanceA = va.dot(planeNormal) - planeDistance;
        distanceB = vb.dot(planeNormal) - planeDistance;

        // If both points are on the back-side of the plane then it is
        // completely clipped away, so return null
        if (distanceA < 0 && distanceB < 0) continue;

        // If both points are on the front-side of the plane then use the line
        // as it is (Point B is omitted because it is the starting point of
        // the next line in the polygon)
        if (distanceA >= 0 && distanceB >= 0)
        {
            this.polygon[j++] = a;
        }
        
        // Otherwise calculate the intersection and use a clipped line
        else
        {    
            // Calculate the intersection point
            s = distanceA / (distanceA - distanceB);
            c = this.origSize + this.extras++;
            if (!this.vertices[c]) this.vertices[c] = new threedee.Vector();
            this.vertices[c].set(va.x + s * (vb.x - va.x), va.y + s
                * (vb.y - va.y), va.z + s * (vb.z - va.z));
    
            // If point A is on the back-side of the plane then use a line
            // from the intersection point to point B (Where point B is
            // omitted because it is the original starting  point of the next
            // line in the polygon
            if (distanceA < 0)
            {
                this.polygon[j++] = c;
            }

            // Otherwise point B is on the back-side so use a line from point A
            // to intersection point
            else
            {
                this.polygon[j++] = a;
                this.polygon[j++] = c;
            }
        }
    }

    this.size = j;
    return !j;
};

/**
 * Returns the number of used vertices.
 * 
 * @return {number} The number of used vertices
 */
threedee.RenderPolygon.prototype.countVertices = function()
{
    return this.size;
};

/**
 * Returns the vertex with the specified index.
 * 
 * @param {number} index
 *            The vertex index
 * @return {!threedee.Vector} The vertex
 */
threedee.RenderPolygon.prototype.getVertex = function(index)
{
    return this.vertices[this.polygon[index]];
};

/**
 * Returns the center of the polygon.
 * 
 * Speed-optimization: The calculated center vector is cached in an instance
 * scope variable to prevent creation of vector objects on reach frame render.
 * WARNING: If you want to use the returned center vector for a longer time
 * (and not only for a single frame) then you should copy the vector because
 * otherwise the content may change when the polygon changes.
 * 
 * @return {!threedee.Vector} The center of the polygon
 */
threedee.RenderPolygon.prototype.getCenter = function()
{
    var vertexCount, ax, ay, az, i, v;
    
    vertexCount = this.size;
    ax = 0;
    ay = 0;
    az = 0;

    for (i = 0; i < vertexCount; i++)
    {
        v = this.vertices[this.polygon[i]];
        ax += v.x;
        ay += v.y;
        az += v.z;
    }
    ax /= vertexCount;
    ay /= vertexCount;
    az /= vertexCount;

    if (!this.center) this.center = new threedee.Vector();
    return this.center.set(ax, ay, az);
};

/**
 * Calculates the normal of the polygon. May return null if polygon has
 * fewer then three vertices.
 * 
 * Speed-optimization: For calculating the normal we need three additional
 * vectors. Two of them (V1 and V2) are only temporary and therefore are
 * shared in static scope. The third one is polygon-specific and
 * is shared in instance scope. WARNING: If you want to use the returned normal
 * vector for a longer time (and not only for a single frame) then you should
 * copy the vector because otherwise the content may change when the polygon
 * changes.
 * 
 * @return {?threedee.Vector}
 *            The normal or null if polygon has fewer then three vertices
 */
threedee.RenderPolygon.prototype.getNormal = function()
{
    var a, b, c, v1, v2;

    // If polygon has fewer then three vertices then it has no normal
    if (this.polygon.length < 3) return null;

    a = this.vertices[this.polygon[0]];
    b = this.vertices[this.polygon[1]];
    c = this.vertices[this.polygon[2]];
    
    v1 = b.copy(threedee.RenderPolygon.V1).sub(a);
    v2 = a.copy(threedee.RenderPolygon.V2).sub(c);
    this.normal = v1.cross(v2).toUnit().copy(this.normal);
    return this.normal;
};

/**
 * Updates the average Z value.
 */
threedee.RenderPolygon.prototype.updateAverageZ = function()
{
    var averageZ, vertexCount, v;
    
    averageZ = 0;
    vertexCount = this.size;
    for (v = 0; v < vertexCount; v++)
    {
        averageZ += this.vertices[this.polygon[v]].z;
    }
    averageZ /= vertexCount;
    this.averageZ = averageZ;
};

/**
 * Compares two polygons according to their average Z value. This is used
 * to sort polygons.
 *
 * @param {!threedee.RenderPolygon} a
 *             The first polygon to compare
 * @param {!threedee.RenderPolygon} b
 *             The second polygon to compare
 * @return {number}
 *             Lower than 0 if z-value of second polygon is lower then z-value
 *             of first polygon. Greater than 0 if the other way around. 0
 *             means z-values are equal 
 */
threedee.RenderPolygon.compare = function(a, b)
{
    return b.averageZ - a.averageZ;
};

/**
 * Checks if currently the backface of the polygon is visible.
 * 
 * @return {boolean}
 *            True if backface is visible, false if front-side.
 */
threedee.RenderPolygon.prototype.isBackface = function()
{
    return this.getNormal().dot(this.getVertex(0)) > 0;
};

/**
 * Returns the polygon material. If it has a specific material then this one
 * is returned. Otherwise the global modal material is returned.
 * 
 * @return {threedee.Material} The material to be used for this polygon
 */
threedee.RenderPolygon.prototype.getMaterial = function()
{
    if (this.material) return this.material;
    return this.model.getMaterial();
};
