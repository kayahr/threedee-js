/**
 * Copyright (C) 2009-2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information
 * 
 * @require threedee.js
 * @use threedee/rendering/RenderPolygon.js
 */

/**
 * Constructs a new render model.
 * 
 * @param {!threedee.Model} model
 *            The original model
 *            
 * @constructor
 * @class A render model is a wrapper around a model which is used internally
 * for rendering the model. It is responsible for transforming the vertices
 * of the model (and caching them) and it also wraps the polygons of the
 * model with RenderPolygon objects.
 */
threedee.RenderModel = function(model)
{
    var i, max;
    
    this.model = model;
    
    this.vertices = [];
    for (i = 0, max = this.model.countVertices(); i < max; i++)
        this.vertices.push(this.model.getVertex(i).copy());
    
    this.polygons = [];
    for (i = 0, max = this.model.countPolygons();  i < max; i++)
        this.polygons.push(new threedee.RenderPolygon(model, this.model.getPolygon(i),
            this.vertices));
};

/** 
 * The vertices. 
 * @private 
 * @type {!Array.<!threedee.Vector>} 
 */
threedee.RenderModel.prototype.vertices;

/** 
 * The polygons. 
 * @private 
 * @type {!Array.<!threedee.RenderPolygon>} 
 */
threedee.RenderModel.prototype.polygons;

/** 
 * The model. 
 * @private 
 * @type {!threedee.Model} 
 */
threedee.RenderModel.prototype.model;

/**
 * Transforms this node model.
 * 
 * @param {!threedee.Matrix} transform 
 *            The transformation matrix
 */
threedee.RenderModel.prototype.transform = function(transform)
{
    var i, max;
    
    // Transform the vertices
    for (i = 0, max = this.vertices.length; i < max; i++)
        this.model.getVertex(i).copy(this.vertices[i]).transform(transform);
};

/**
 * Returns the number of polygons.
 * 
 * @return {number} The number of polygons
 */
threedee.RenderModel.prototype.countPolygons = function()
{
    return this.polygons.length;
};

/**
 * Returns the polygon with the specified index.
 *
 * @param {number} index
 *            The polygon index
 * @return {!threedee.RenderPolygon} The polygon
 */
threedee.RenderModel.prototype.getPolygon = function(index)
{
    return this.polygons[index];
};
