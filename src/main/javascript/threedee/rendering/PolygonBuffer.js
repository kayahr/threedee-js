/**
 * Copyright (C) 2009-2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information
 * 
 * @require threedee.js
 * @require threedee/Color.js
 * @require threedee/Vector.js
 * @use threedee/rendering/FpsCounter.js
 * @use threedee/rendering/RenderOptions.js
 * @use threedee/rendering/TransformedLight.js
 * @use threedee/rendering/Frustum.js
 */

/**
 * @constructor
 * Constructs a new polygon buffer.
 *            
 * @class
 * A polygon buffer
 */

threedee.PolygonBuffer = function()
{
    this.vertices = [];
    this.polygons = [];
    this.lights = [];
    this.fpsCounter = new threedee.FpsCounter();
    this.renderOptions = new threedee.RenderOptions();
};

/** 
 * Speed-optimization vector cache. 
 * @private 
 * @type {!threedee.Vector} 
 */
threedee.PolygonBuffer.V = new threedee.Vector();

/**
 * The render options.
 * @private
 * @type {!threedee.RenderOptions}
 */
threedee.PolygonBuffer.prototype.renderOptions;

/** 
 * The last gathered debug info. 
 * @private 
 * @type {string} 
 */
threedee.PolygonBuffer.prototype.debugInfo = "";

/** 
 * The vertices in the buffer. 
 * @private 
 * @type {!Array.<!threedee.Vector>} 
 */
threedee.PolygonBuffer.prototype.vertices;

/** 
 * The polygons in the buffer. 
 * @private 
 * @type {!Array.<!threedee.RenderPolygon>} 
 */
threedee.PolygonBuffer.prototype.polygons;

/** 
 * The registered light sources. 
 * @private 
 * @type {!Array.<!threedee.TransformedLight>} 
 */
threedee.PolygonBuffer.prototype.lights;

/** 
 * The screen width. 
 * @private 
 * @type {number} 
 */
threedee.PolygonBuffer.prototype.width = 0;

/** 
 * The screen height. 
 * @private 
 * @type {number} 
 */
threedee.PolygonBuffer.prototype.height = 0;

/** 
 * The factor used for scaling the perspective view transformation. 
 * @private 
 * @type {number} 
 */
threedee.PolygonBuffer.prototype.factor = 1;

/** 
 * The view frustum. 
 * @private 
 * @type {!threedee.Frustum} */
threedee.PolygonBuffer.prototype.frustum;

/** 
 * The FPS counter. 
 * @private 
 * @type {!threedee.FpsCounter} 
 */
threedee.PolygonBuffer.prototype.fpsCounter;

/** 
 * The global ambient color. 
 * @private 
 * @type {!threedee.Color} 
 */
threedee.PolygonBuffer.prototype.globalAmbient = threedee.Color.DARK_GRAY;

/**
 * Sets the global ambient color.
 * 
 * @param {!threedee.Color} globalAmbient
 *            The global ambient color to set
 */
threedee.PolygonBuffer.prototype.setGlobalAmbient = function(globalAmbient)
{
    this.globalAmbient = globalAmbient;
};

/**
 * Sets the render options.
 * 
 * @param {!threedee.RenderOptions} renderOptions
 *            The render options to set.
 */
threedee.PolygonBuffer.prototype.setRenderOptions = function(renderOptions)
{
    this.renderOptions = renderOptions;
};

/**
 * Prepares the polygon buffer for the next use.
 * 
 * @param {number} width
 *            The output width in pixels.
 * @param {number} height
 *            The output height in pixels.
 */
threedee.PolygonBuffer.prototype.prepare = function(width, height)
{
    var eyeDistance, screenInMeters, screenInPixels, dpm;
    
    // Reset the buffer
    this.vertices = [];
    this.polygons = [];
    this.lights = [];

    // Setup the view frustum (Repeated when output size changes)
    if (width != this.width || height != this.height)
    {
        eyeDistance = 0.5; // Eye is 0.5 meters from screen
        screenInMeters = 0.38;
        screenInPixels = Math.min(width, height);
        dpm = screenInPixels / screenInMeters;
        this.factor = eyeDistance * dpm;
    
        // Generate the view frustum for clipping the polygons
        this.frustum = new threedee.Frustum(width, height, this.factor);
    
        // Remember output size
        this.width = width;
        this.height = height;
    }
};

/**
 * Adds the specified model to the polygon buffer.
 * 
 * @param {!threedee.RenderModel} model
 *            The model to add
 * @param {!threedee.Matrix} transform
 *            The transformation matrix to use
 */
threedee.PolygonBuffer.prototype.addModel = function(model, transform)
{
    var backfaceCulling, i, max, polygon;
    
    // Pull render options into local variables
    backfaceCulling = this.renderOptions.backfaceCulling;
    
    // Inform the model about the transformation if it is a LoD model
    //if (model instanceof threedee.LoDModel)
        //model.prepareLoD(transform, this.factor);

    // Transform the vertices of the model
    model.transform(transform);
    
    // Create the transformed polygons and add them to the buffer
    for (i = 0, max = model.countPolygons(); i < max; i++)
    {
        polygon = model.getPolygon(i);
        
        polygon.init();

        // Perform back-face culling
        if (backfaceCulling && polygon.isBackface()) continue;

        // Clip the polygon with the frustum. If it was completely clipped
        // away then ignore it
        if (this.frustum.clipPolygon(polygon)) continue;

        // Calculate the average Z position of the polygon so it can be
        // z-sorted later
        polygon.updateAverageZ();
        
        // Add the polygon to the buffer
        this.polygons.push(polygon);        
    }
};

/**
 * Adds the specified light to the polygon buffer.
 * 
 * @param {!threedee.Light} light
 *            The light to add
 * @param {!threedee.Matrix} transform
 *            The transformation matrix to use
 */
threedee.PolygonBuffer.prototype.addLight = function(light, transform)
{
    this.lights.push(new threedee.TransformedLight(light, transform));
};

/**
 * Renders the polygon buffer.
 * 
 * @param {!CanvasRenderingContext2D} g
 *            The graphics context
 */
threedee.PolygonBuffer.prototype.render = function(g)
{
    var x, y, factor, i, max, vertexCount, polygon, v, vertex, dx, dy, dz,
        displayNormals, lighting, solid, normal, center, normalEnd, cx, cy,
        cx2, cy2, sortZ, debugInfo, fpsInfo, polygonCounter, vertexCounter;

    // Pull render options in local variables
    displayNormals = this.renderOptions.displayNormals;
    sortZ = this.renderOptions.sortZ;
    lighting = this.renderOptions.lighting;
    solid = this.renderOptions.solid;
    debugInfo = this.renderOptions.debugInfo;
    fpsInfo = this.renderOptions.fpsInfo;
    
    // Initialize counters if needed
    polygonCounter = 0;
    vertexCounter = 0;
    
    // Do Z-sorting
    if (sortZ) this.polygons.sort(threedee.RenderPolygon.compare);

    // Remember the old transformation and then apply the screen center
    // transformation
    g.save();
    g.translate(this.width / 2, this.height / 2);

    // Get scale factor
    factor = this.factor;

    // Set the default stroke color
    g.strokeStyle = "#fff";

    // Draw the polygons
    for (i = 0, max = this.polygons.length; i < max; i++)
    {
        polygon = this.polygons[i];
        
        // Project the 3D vertices into 2D coordinates and draw the
        // polygon
        vertexCount = polygon.countVertices();
        g.beginPath();
        for (v = 0; v < vertexCount; v++)
        {
            vertex = polygon.getVertex(v);

            dx = vertex.x;
            dy = vertex.y;
            dz = vertex.z;
    
            x = dx * factor / dz;
            y = -dy * factor / dz;

            if (v)
                g.lineTo(x, y);
            else
                g.moveTo(x, y);
        }
        g.closePath();

        // Set fill or stroke color (Depends on if solid polygons are rendered)
        if (solid)
        {
            if (lighting)
                this.applyPolygonColor(polygon, g);
            else
                g.fillStyle = polygon.getMaterial().getDiffuse().toCSS();
            g.fill();
            if (this.renderOptions.outline)
            {
                g.strokeStyle = this.renderOptions.outlineColor || g.fillStyle;
                g.stroke();
            }
        }
        else
        {
            g.stroke();
        }
        
        // Display normals if needed
        if (displayNormals)
        {
            normal = polygon.getNormal();
            g.save();
            g.strokeStyle = threedee.Color.YELLOW.toCSS();
            center = polygon.getCenter();
            normalEnd = normal.add(center);
            cx = center.x * factor / center.z;
            cy = -center.y * factor  / center.z;
            cx2 = normalEnd.x * factor  / normalEnd.z;
            cy2 = -normalEnd.y * factor  / normalEnd.z;
            g.beginPath();
            g.moveTo(cx, cy);
            g.lineTo(cx2, cy2);
            g.stroke();
            g.restore();
        }       
        
        // Update counters if needed
        if (debugInfo)
        {
            polygonCounter++;
            vertexCounter += vertexCount; 
        }
    }
    
    // Gather debugging info if requested
    if (fpsInfo || debugInfo)
    {
        if (fpsInfo)
        {
            this.fpsCounter.frame();
            this.debugInfo = "Frames/s: " + this.fpsCounter.getFps();
            if (debugInfo) this.debugInfo += "\n\n";
        }
        else this.debugInfo = "";
        
        if (debugInfo)
        {
            this.debugInfo += "Vertices: " + vertexCounter +
                "\nPolygons: " + polygonCounter +
                "\n\nNew objects for this frame:" +
                "\n  Vector: " + threedee.Vector.count() +
                "\n  Matrix: " + threedee.Matrix.count() +
                "\n  Color: " + threedee.Color.count() +
                "\n  Material: " + threedee.Material.count() +
                "\n  Polygon: " + threedee.Polygon.count();
        }
    }

    // Restore the original transformation
    g.restore();
};

/**
 * Calculates the polygon color according to the material and the light
 * sources and applies it to the specified graphics context.
 * 
 * @param {!threedee.RenderPolygon} polygon
 *            The polygon
 * @param {!CanvasRenderingContext2D} g       
 *            The graphics context
 */
threedee.PolygonBuffer.prototype.applyPolygonColor = function(polygon, g)
{
    var material, position, normal, globalAmbient, ambient, diffuse, emissive,
        result, addedDiffuse, i, j, max, transLight, light, lightColor,
        lightPosition, L, diffuseLight;
    
    // Get the material from the polygon
    material = polygon.getMaterial();

    // Get the position (The center) and the normal from the polygon
    position = polygon.getCenter();
    normal = polygon.getNormal();

    // Get the color components of the global ambient color
    globalAmbient = this.globalAmbient.getComponents();

    // Get the ambient, diffuse and emissive color components of the
    // material
    ambient = material.getAmbient().getComponents();
    diffuse = material.getDiffuse().getComponents();
    emissive = material.getEmissive().getComponents();

    // We store the resulting color components here:
    result = new Array(3);

    // We store the added diffuse color components here:
    addedDiffuse = (/** @type {!Array.<number>} */ []);
    addedDiffuse[0] = 0;
    addedDiffuse[1] = 0;
    addedDiffuse[2] = 0;

    // Now we cycle through all color components and calculate the values
    for (i = 0; i < 3; i++)
    {
        // Calculate the ambient color value
        ambient[i] = ambient[i] * globalAmbient[i];

        // Iterate over all registered light sources and process them
        for (j = 0, max = this.lights.length; j < max; j++)
        {
            transLight = this.lights[j];
            light = transLight.getLight();
            if (light instanceof threedee.PointLight)
            {
                lightColor = light.getColor().getComponent(i);

                lightPosition = transLight.getPosition().copy(threedee.PolygonBuffer.V);
                L = lightPosition.sub(position).toUnit();
                diffuseLight = Math.max(normal.dot(L), 0);

                addedDiffuse[i] += diffuse[i] * lightColor * diffuseLight;
            }
        }

        result[i] = Math.min(ambient[i] + emissive[i] + addedDiffuse[i], 1);
    }

    // Apply color to graphics context
    g.fillStyle = "rgb(" +
        parseInt(result[0] * 255, 10) + "," +
        parseInt(result[1] * 255, 10) + "," +
        parseInt(result[2] * 255, 10) + ")";
};


/**
 * Returns debugging info. The debugInfo flag in the rendering options must
 * be set to true to get up-to-date debug info.
 * 
 * @return {string} The debug info
 */

threedee.PolygonBuffer.prototype.getDebugInfo = function()
{
    return this.debugInfo;
};
