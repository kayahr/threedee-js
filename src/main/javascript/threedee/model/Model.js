/**
 * Copyright (C) 2009-2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information
 * 
 * @require threedee.js
 * @require threedee/model/Material.js
 * @use threedee/model/Polygon.js
 * @use threedee/Vector.js */

/**
 * Constructs a new model with the specified vertices and polygons.
 * 
 * @param {!Array.<!threedee.Vector>} vertices
 *            The model vertices
 * @param {!Array.<!threedee.Polygon>} polygons
 *            The model polygons
 * @param {?threedee.Material=} material
 *            Optional material.
 * @constructor
 * @class 
 * The base class for all models.
 */
threedee.Model = function(vertices, polygons, material)
{
    this.vertices = vertices;
    this.polygons = polygons;
    if (material) this.material = material;
};

/** 
 * The vertices. 
 * @private 
 * @type {!Array.<!threedee.Vector>} 
 */
threedee.Model.prototype.vertices;

/** 
 * The polygons. 
 * @private 
 * @type {!Array.<!threedee.Polygon>} 
 */
threedee.Model.prototype.polygons;

/** 
 * The material. 
 * @private 
 * @type {!threedee.Material} 
 */
threedee.Model.prototype.material = threedee.Material.DEFAULT;

/**
 * Returns the number of vertices used in this model.
 * 
 * @return {number} The number of vertices used in this model
 */
threedee.Model.prototype.countVertices = function()
{
    return this.vertices.length;
};

/**
 * Returns the vertex with the specified index.
 * 
 * @param {number} index
 *            The index
 * @return {!threedee.Vector}
 *            The vertex
 */
threedee.Model.prototype.getVertex = function(index)
{
    return this.vertices[index];
};

/**
 * Returns the number of polygons used in this model.
 * 
 * @return {number} The number of polygons used in this model
 */
threedee.Model.prototype.countPolygons = function()
{
    return this.polygons.length;
};

/**
 * Returns the polygon with the specified index.
 * 
 * @param {number} index
 *            The index
 * @return {!threedee.Polygon} The polygon
 */
threedee.Model.prototype.getPolygon = function(index)
{
    return this.polygons[index];
};

/**
 * Returns the material of the model.
 * 
 * @return {threedee.Material}
 *            The material of the model. Never null.
 */
threedee.Model.prototype.getMaterial = function()
{
    return this.material;
};

/**
 * Converts the model into a JSON object with keys 'm', 'v' and 'p'.
 * 
 * @return {Object} 
 *            The model as a JSON object.
 */
threedee.Model.prototype.toJSON = function()
{
    var data, vertices, polygons, i, max;
    
    vertices = [];
    polygons = [];
    
    for (i = 0, max = this.vertices.length; i < max; i++)
        vertices.push(this.vertices[i].toJSON());
    for (i = 0, max = this.polygons.length; i < max; i++)
        polygons.push(this.polygons[i].toJSON());
    
    data = { "v": vertices, "p": polygons };
    if (this.material) data.m = this.material.toJSON();
    return data;
};

/**
 * Creates a new model instance with the data read from the
 * specified JSON object (with keys 'm' (Global model material), 'v' (Vertices)
 * and 'p' (Polygons)). Returns null if data
 * was empty.
 * 
 * @param {{m:?Object,v:!Array.<!{x:number,y:number,z:number}>,p:!Array.<!Object>}} data
 *            The model as JSON object.
 * @return {!threedee.Model} The model object or null if data was empty.
 */
threedee.Model.fromJSON = function(data)
{
    var material, i, max, vertices, polygons;
    
    // Parse polygons
    polygons = [];
    for (i = 0, max = data.p.length; i < max; i++)
        polygons.push(threedee.Polygon.fromJSON(data.p[i]));

    // Parse vertexes
    vertices = [];
    for (i = 0, max = data.v.length; i < max; i++)
        vertices.push(threedee.Vector.fromJSON(data.v[i]));
    
    // Parse model material
    material = threedee.Material.fromJSON(data.m);

    // Construct the model
    return new threedee.Model(vertices, polygons, material);
};
