/**
 * Copyright (C) 2009-2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information
 * 
 * @require threedee.js
 * @require threedee/model/Model.js
 * @use threedee/Vector.js
 * @use threedee/model/Polygon.js
 */

/**
 * Constructs a new cube with the specified sizes.
 * 
 * @param {number} xRadius
 *            The X radius
 * @param {number=} yRadius
 *            The Y radius. Optional. Defaults to X radius.
 * @param {number=} zRadius
 *            The Z radius. Optional. Defaults to Y radius.
 *            
 * @constructor
 * @extends {threedee.Model}
 * @class
 * A cube model.
 */
threedee.Cube = function(xRadius, yRadius, zRadius)
{
    if (yRadius === undefined) yRadius = xRadius;
    if (zRadius === undefined) zRadius = xRadius;

    threedee.Model.call(this, [
            new threedee.Vector(-xRadius, yRadius, zRadius),
            new threedee.Vector(-xRadius, yRadius, -zRadius),
            new threedee.Vector(xRadius, yRadius, -zRadius),
            new threedee.Vector(xRadius, yRadius, zRadius),
            new threedee.Vector(-xRadius, -yRadius, zRadius),
            new threedee.Vector(-xRadius, -yRadius, -zRadius),
            new threedee.Vector(xRadius, -yRadius, -zRadius),
            new threedee.Vector(xRadius, -yRadius, zRadius)
        ],
        [
            new threedee.Polygon([ 0, 1, 2, 3 ]),
            new threedee.Polygon([ 7, 3, 2, 6 ]),
            new threedee.Polygon([ 4, 7, 6, 5 ]),
            new threedee.Polygon([ 1, 0, 4, 5 ]),
            new threedee.Polygon([ 0, 3, 7, 4 ]),
            new threedee.Polygon([ 6, 2, 1, 5 ])
        ]
    );
};
threedee.inherit(threedee.Cube, threedee.Model);
