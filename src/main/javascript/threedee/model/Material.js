/**
 * Copyright (C) 2009-2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information
 * 
 * @require threedee.js
 * @require threedee/Color.js
 */

/**
 * Constructs a new Material with the specified properties.
 * 
 * @param {threedee.Color=} ambient
 *            Optional ambient color. Defaults to white.
 * @param {threedee.Color=} diffuse
 *            Optional diffuse color. Defaults to white.
 * @param {threedee.Color=} emissive
 *            Optional emissive color. Defaults to black.
 *            
 * @constructor
 * @class 
 * A material.
 */
threedee.Material = function(ambient, diffuse, emissive)
{
    if (ambient) this.ambient = ambient;
    if (diffuse) this.diffuse = diffuse;
    if (emissive) this.emissive = emissive;
    threedee.Material.counter++;
};

/** 
 * Instance counter. 
 * @private 
 * @type {number} 
 */
threedee.Material.counter = 0; 
    
/** 
 * The default material. 
 * @final 
 * @type {!threedee.Material} 
 */
threedee.Material.DEFAULT = new threedee.Material();

/** 
 * The ambient color. 
 * @private 
 * @type {!threedee.Color} 
 */
threedee.Material.prototype.ambient = threedee.Color.WHITE;

/** 
 * The emissive color. 
 * @private
 * @type {!threedee.Color} 
 */
threedee.Material.prototype.diffuse = threedee.Color.WHITE;

/** 
 * The diffuse color. 
 * @private 
 * @type {!threedee.Color} 
 */
threedee.Material.prototype.emissive = threedee.Color.BLACK;

/**
 * Returns and resets the current instance counter.
 * 
 * @return {number} The number of created instances since the last call
 */
threedee.Material.count = function()
{
    var value = threedee.Material.counter;
    threedee.Material.counter = 0;
    return value;
};


/**
 * Returns the ambient color.
 * 
 * @return {threedee.Color} The ambient color
 */

threedee.Material.prototype.getAmbient = function()
{
    return this.ambient;
};


/**
 * Returns the diffuse color.
 * 
 * @return {threedee.Color} The diffuse color
 */

threedee.Material.prototype.getDiffuse = function()
{
    return this.diffuse;
};


/**
 * Returns the emissive color.
 * 
 * @return {threedee.Color} The emissive color
 */

threedee.Material.prototype.getEmissive = function()
{
    return this.emissive;
};


/**
 * Converts the material into a JSON object with keys 'a', 'd' and 'e' and
 * returns it.
 * 
 * @return {Object} The material as a JSON object
 */

threedee.Material.prototype.toJSON = function()
{
    return { "a": this.ambient.toJSON(), "d": this.diffuse.toJSON(),
        "e": this.emissive.toJSON() };
};


/**
 * Creates a new material instance with the data read from the
 * specified JSON object (with keys 'a', 'd' and 'e'). Returns null if data
 * was empty.
 * 
 * @param {Object} data
 *            The material as JSON object
 * @return {threedee.Material} The material object or null if data was empty
 */

threedee.Material.fromJSON = function(data)
{
    if (!data) return null;
    return new threedee.Material(
        threedee.Color.fromJSON(data["a"]),
        threedee.Color.fromJSON(data["d"]),
        threedee.Color.fromJSON(data["e"]));
};
