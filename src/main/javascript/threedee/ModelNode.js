/**
 * Copyright (C) 2009-2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information
 * 
 * @require threedee.js
 * @require threedee/SceneNode.js
 * @use threedee/rendering/RenderModel.js
 */

/**
 * Constructs a new model node.
 * 
 * @param {!threedee.Model} model
 *            The model
 *            
 * @constructor
 * @extends {threedee.SceneNode}
 * @class
 * A node which draws a model.
 */

threedee.ModelNode = function(model)
{
    threedee.SceneNode.call(this);
    this.model = new threedee.RenderModel(model);
};
threedee.inherit(threedee.ModelNode, threedee.SceneNode);

/** 
 * The model.
 * @private 
 * @type {!threedee.RenderModel} 
 */
threedee.ModelNode.prototype.model;

/**
 * @inheritDoc
 * @param {!threedee.PolygonBuffer} buffer
 * @param {!threedee.Matrix} transform
 */
threedee.ModelNode.prototype.render = function(buffer, transform)
{
    buffer.addModel(this.model, transform);
    threedee.SceneNode.prototype.render.call(this, buffer, transform);
};
