/**
 * Copyright (C) 2009-2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information
 * 
 * @require threedee.js
 * @require threedee/updater/NodeUpdater.js
 */

/**
 * Constructs a new roll updater.
 *  
 * @param {number} angle
 *            The angle the node should be rotated around the Z axis per
 *            second. Measured in clock-wise RAD.
 *            
 * @constructor
 * @extends {threedee.NodeUpdater}
 * @class
 * The roll updater rotates a node around the Z axis.
 */
threedee.RollUpdater = function(angle)
{
    threedee.NodeUpdater.call(this);
    this.angle = angle;
};
threedee.inherit(threedee.RollUpdater, threedee.NodeUpdater);

/** 
 * The rotation angle in clock-wise RAD per second.
 * @private
 * @type {number} 
 */
threedee.RollUpdater.prototype.angle;

/**
 * Returns the current angle.
 * 
 * @return {number}
 *            The angle the node is rotated around the X axis per second.
 *            Measured in clock-wise RAD.
 */
threedee.RollUpdater.prototype.getAngle = function()
{
    return this.angle;
};

/**
 * Sets the angle.
 * 
 * @param {number} angle
 *            The angle the node should be rotated around the X axis per
 *            second. Measured in clock-wise RAD.
 */
threedee.RollUpdater.prototype.setAngle = function(angle)
{
    this.angle = angle;
};

/**
 * @inheritDoc
 * 
 * @param {!threedee.SceneNode} node
 * @param {number} delta
 */
threedee.RollUpdater.prototype.update = function(node, delta)
{
    // Do nothing if angle is 0
    if (this.angle == 0) return;

    node.getTransform().rotateZ(this.angle * delta / 1000);
};
