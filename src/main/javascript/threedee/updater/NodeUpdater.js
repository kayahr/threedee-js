/**
 * Copyright (C) 2009-2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information
 * 
 * @require threedee.js
 */

/**
 * @constructor
 * Constructs a new updater.
 * 
 * @class
 * A plane
 */
threedee.NodeUpdater = function()
{
    // Empty
};

/**
 * Updates the node with the specified time delta.
 * 
 * @param {!threedee.SceneNode} node
 *            The node to update
 * @param {number} delta
 *            The time elapsed since the last scene update (in milliseconds)
 */
threedee.NodeUpdater.prototype.update = function(node, delta)
{
    // Empty
};
