/**
 * Copyright (C) 2009-2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information
 * 
 * @require threedee.js
 * @require threedee/updater/NodeUpdater.js
 */

/**
 * Constructs a new yaw updater.
 * 
 * @param {number} angle
 *            The angle the node should be rotated around the Y axis per
 *            second. Measured in clock-wise RAD.
 * @constructor
 * @extends {threedee.NodeUpdater}
 * @class
 * The yaw updater rotates a node around the Y axis.
 */
threedee.YawUpdater = function(angle)
{
    threedee.NodeUpdater.call(this);
    this.angle = angle;
};
threedee.inherit(threedee.YawUpdater, threedee.NodeUpdater);

/** 
 * The rotation angle in clock-wise RAD per second.
 * @private
 * @type {number}
 */
threedee.YawUpdater.prototype.angle;

/**
 * Returns the current angle.
 * 
 * @return {number}
 *            The angle the node is rotated around the X axis per second.
 *            Measured in clock-wise RAD.
 */
threedee.YawUpdater.prototype.getAngle = function()
{
    return this.angle;
};

/**
 * Sets the angle.
 * 
 * @param {number} angle
 *            The angle the node should be rotated around the X axis per
 *            second. Measured in clock-wise RAD.
 */
threedee.YawUpdater.prototype.setAngle = function(angle)
{
    this.angle = angle;
};

/**
 * @inheritDoc
 * 
 * @param {!threedee.SceneNode} node
 * @param {number} delta
 */
threedee.YawUpdater.prototype.update = function(node, delta)
{
    // Do nothing if angle is 0
    if (this.angle == 0) return;

    node.getTransform().rotateY(this.angle * delta / 1000);
};
