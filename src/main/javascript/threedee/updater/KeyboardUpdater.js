/**
 * Copyright (C) 2009-2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information
 * 
 * @require threedee.js
 * @require threedee/updater/NodeUpdater.js
 */

/**
 * Constructs a new keyboard updater connected to the specified HTML element.
 * 
 * @param {(Window|Element)=} element
 *            The element to which the keyboard event handlers are connected.
 *            This is optional and defaults to the browser window.
 *            
 * @constructor
 * @extends {threedee.NodeUpdater}
 * @class 
 * This updater can be bound to a HTML element (Window is default) where
 * it listens for key presses and releases to transform the connected node
 * according to the key pressed and velocity settings.
 */

threedee.KeyboardUpdater = function(element)
{
    var updater;
    
    threedee.NodeUpdater.call(this);
    if (!element) element = window;
    this.element = element;
    
    updater = this;
    
    element.addEventListener("keydown", this.handleKeyDown.bind(this), false);     
    element.addEventListener("keyup", this.handleKeyUp.bind(this), false);     
};
threedee.inherit(threedee.KeyboardUpdater, threedee.NodeUpdater);

/**
 * The element to which the keyboard event handlers are connected.
 * @private
 * @type {!(Window|Element)}
 */
threedee.KeyboardUpdater.prototype.element;

/**
 * The movement speed in units per second.
 * @private
 * @type {number}
 */ 
threedee.KeyboardUpdater.prototype.speed = 10;

/** 
 * The rotation speed in clock-wise RAD per second. 
 * @private
 * @type {number}
 */
threedee.KeyboardUpdater.prototype.rotSpeed = 45 * Math.PI / 180;

/**
 * Flag indicating if the left key has been pressed.
 * @private
 * @type {boolean}
 */
threedee.KeyboardUpdater.prototype.left = false;

/**
 * Flag indicating if the right key has been pressed.
 * @private
 * @type {boolean}
 */
threedee.KeyboardUpdater.prototype.right = false;

/**
 * Flag indicating if the up key has been pressed.
 * @private
 * @type {boolean}
 */
threedee.KeyboardUpdater.prototype.up = false;

/**
 * Flag indicating if the down key has been pressed.
 * @private
 * @type {boolean}
 */
threedee.KeyboardUpdater.prototype.down = false;

/**
 * Flag indicating if the forward key has been pressed.
 * @private
 * @type {boolean}
 */
threedee.KeyboardUpdater.prototype.forward = false;

/**
 * Flag indicating if the backward key has been pressed.
 * @private
 * @type {boolean}
 */
threedee.KeyboardUpdater.prototype.backward = false;

/**
 * Flag indicating if the pitch up key has been pressed.
 * @private
 * @type {boolean}
 */
threedee.KeyboardUpdater.prototype.pitchUp = false;

/**
 * Flag indicating if the pitch down key has been pressed.
 * @private
 * @type {boolean}
 */
threedee.KeyboardUpdater.prototype.pitchDown = false;

/**
 * Flag indicating if the yaw left key has been pressed.
 * @private
 * @type {boolean}
 */
threedee.KeyboardUpdater.prototype.yawLeft = false;

/**
 * Flag indicating if the yaw right key has been pressed.
 * @private
 * @type {boolean}
 */
threedee.KeyboardUpdater.prototype.yawRight = false;

/**
 * Flag indicating if the roll left key has been pressed.
 * @private
 * @type {boolean}
 */
threedee.KeyboardUpdater.prototype.rollLeft = false;

/**
 * Flag indicating if the roll right key has been pressed.
 * @private
 * @type {boolean}
 */
threedee.KeyboardUpdater.prototype.rollRight = false;

/**
 * Handles the key-down event.
 * 
 * @param {!Event} e
 *           The event.
 * @private
 */
threedee.KeyboardUpdater.prototype.handleKeyDown = function(e)
{
    switch (e.keyCode)
    {
        case 87:
            this.forward = true;
            break;
            
        case 83:
            this.backward = true;
            break;
                        
        case 65:
            this.left = true;
            break;

        case 68:
            this.right = true;
            break;

        case 82:
            this.down = true;
            break;

        case 70:
            this.up = true;
            break;
            
        case 37:
            this.yawLeft = true;
            break;
            
        case 39:
            this.yawRight = true;
            break;

        case 38:
            this.pitchUp = true;
            break;
            
        case 40:
            this.pitchDown = true;
            break;

        case 81:
            this.rollLeft = true;
            break;
            
        case 69:
            this.rollRight = true;
            break;

        default:
            return;
    }
    e.preventDefault();
};

/**
 * Handles the key-up event.
 * 
 * @param {!Event} e
 *           The event.
 * @private
 */
threedee.KeyboardUpdater.prototype.handleKeyUp = function(e)
{
    switch (e.keyCode)
    {
        case 87:
            this.forward = false;
            break;
            
        case 83:
            this.backward = false;
            break;
            
        case 65:
            this.left = false;
            break;

        case 68:
            this.right = false;
            break;

        case 82:
            this.down = false;
            break;

        case 70:
            this.up = false;
            break;

        case 37:
            this.yawLeft = false;
            break;
            
        case 39:
            this.yawRight = false;
            break;

        case 38:
            this.pitchUp = false;
            break;
            
        case 40:
            this.pitchDown = false;
            break;

        case 81:
            this.rollLeft = false;
            break;
            
        case 69:
            this.rollRight = false;
            break;

        default:
    }
    e.preventDefault();
};

/**
 * @inheritDoc
 * 
 * @param {!threedee.SceneNode} node
 * @param {number} delta
 */
threedee.KeyboardUpdater.prototype.update = function(node, delta)
{
    var x, y, z, rx, ry, rz, transform;
    
    x = this.right ? 1 : (this.left ? -1 : 0);
    y = this.down ? 1 : (this.up ? -1 : 0);
    z = this.forward ? 1 : (this.backward ? -1 : 0);
    rx = this.pitchUp ? 1 : (this.pitchDown ? -1 : 0);
    ry = this.yawRight ? 1 : (this.yawLeft ? -1 : 0);
    rz = this.rollLeft ? 1 : (this.rollRight ? -1 : 0);

    transform = node.getTransform();
    
    transform.translate(x * this.speed * delta / 1000, y * this.speed
        * delta / 1000, z * this.speed * delta / 1000);
    
    transform.rotateX(rx * this.rotSpeed * delta / 1000);               
    transform.rotateY(ry * this.rotSpeed * delta / 1000);               
    transform.rotateZ(rz * this.rotSpeed * delta / 1000);
};
