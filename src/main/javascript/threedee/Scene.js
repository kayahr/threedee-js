/**
 * Copyright (C) 2009-2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information
 * 
 * @require threedee.js
 * @require threedee/Color.js
 * @use threedee/rendering/PolygonBuffer.js
 */

/**
 * Constructs a new scene.
 *            
 * @constructor
 * @class
 * A scene.
 */
threedee.Scene = function()
{
    this.buffer = new threedee.PolygonBuffer();
};

/** 
 * The root node of the scene.
 * @private 
 * @type {?threedee.SceneNode} 
 */
threedee.Scene.prototype.rootNode = null;

/** 
 * The polygon buffer. 
 * @private 
 * @type {!threedee.PolygonBuffer} 
 */
threedee.Scene.prototype.buffer;

/** 
 * The global ambient color. 
 * @private 
 * @type {!threedee.Color} 
 */
threedee.Scene.prototype.globalAmbient = threedee.Color.DARK_GRAY;

/** 
 * The last update timestamp. 
 * @private 
 * @type {number} 
 */
threedee.Scene.prototype.lastUpdate = 0;

/**
 * Sets the root node.
 * 
 * @param {?threedee.SceneNode} rootNode
 *            The root node to set. Null to unset.
 */
threedee.Scene.prototype.setRootNode = function(rootNode)
{
    this.rootNode = rootNode;
};

/**
 * Returns the current root node.
 * 
 * @return {?threedee.SceneNode}
 *            The current root node. Null if none set.
 */
threedee.Scene.prototype.getRootNode = function()
{
    return this.rootNode;
};

/**
 * Sets the global ambient color.
 * 
 * @param {!threedee.Color} globalAmbient
 *            The global ambient color to set
 */
threedee.Scene.prototype.setGlobalAmbient = function(globalAmbient)
{
    this.globalAmbient = globalAmbient;
};

/**
 * Returns the global ambient color.
 * 
 * @return {!threedee.Color} The global ambient color
 */
threedee.Scene.prototype.getGlobalAmbient = function()
{
    return this.globalAmbient;
};

/**
 * Updates the scene with the specified time delta (milliseconds);
 * 
 * @param {number} delta
 *            The time elapsed since the last call to this method measured
 *            in milliseconds. This is optional. If not specified then
 *            it is calculated automatically
 */
threedee.Scene.prototype.update = function(delta)
{
    var now;
    
    if (!this.rootNode) return;

    if (delta === undefined)
    {
        now = new Date().getTime();
        delta = now - this.lastUpdate;
        this.lastUpdate = now;
        
        // If delta is too large then ignore this update
        if (delta > 10000) return;
    }
    
    // Update the root node
    this.rootNode.update(delta);  
};


/**
 * Renders the scene.
 * 
 * @param {!CanvasRenderingContext2D} g
 *            The graphics context
 * @param {number} width
 *            The output width in pixels
 * @param {number} height
 *            The output height in pixels
 * @param {?threedee.CameraNode=} camera
 *            The camera node to use. If it is null or not specified at all
 *            then a fixed default camera at position 0,0,0 looking in 
 *            direction 0,0,1 is used.
 * @param {?threedee.RenderOptions=} renderOptions
 *            The render options. If not set then the default options
 *            are used.
 */
threedee.Scene.prototype.render = function(g, width, height,
    camera, renderOptions)
{
    var rootTransform;
    
    // If no root node is set yet then do nothing
    if (!this.rootNode) return;

    // Calculate the root transformation
    rootTransform = this.rootNode.getTransform();
    if (camera)
    {
        rootTransform =
            camera.getTransform().copy().invert()
                .transform(rootTransform);
    }

    // Initialize the polygon buffer and recursively render the scene
    // nodes into it
    this.buffer.prepare(width, height);
    this.buffer.setGlobalAmbient(this.globalAmbient);
    if (renderOptions) this.buffer.setRenderOptions(renderOptions);
    this.rootNode.render(this.buffer, rootTransform);

    // Render the polygon buffer onto the screen
    this.buffer.render(g);
};

/**
 * Returns debugging info. The debugInfo flag in the rendering options must
 * be set to true to get up-to-date debug info.
 * 
 * @return {string} The debug info
 */
threedee.Scene.prototype.getDebugInfo = function()
{
    return this.buffer.getDebugInfo();
};
