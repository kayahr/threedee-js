/**
 * Copyright (C) 2009-2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information
 * 
 * @require threedee.js
 */

/**
 * Constructs a new light.
 * @constructor
 * @class
 * Base class for lights.
 */
threedee.Light = function()
{
    // Empty
};
