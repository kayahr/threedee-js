/**
 * Copyright (C) 2009-2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information
 * 
 * @require threedee.js
 * @require threedee/Color.js
 * @require threedee/light/Light.js
 */

/**
 * Constructs a new point light
 * 
 * @param {threedee.Color=} color
 *            Optional light color. Defaults to white.
 *            
 * @constructor
 * @extends {threedee.Light}
 * @class A Point light
 */
threedee.PointLight = function(color)
{
    if (color) this.color = color;
};
threedee.inherit(threedee.PointLight, threedee.Light);

/** 
 * The light color. 
 * @private 
 * @type {!threedee.Color} 
 */
threedee.PointLight.prototype.color = threedee.Color.WHITE;

/**
 * Sets the light color
 * 
 * @param {!threedee.Color} color
 *            The light color to set.
 */
threedee.PointLight.prototype.setColor = function(color)
{
    this.color = color;
};

/**
 * Returns the light color
 * 
 * @return {!threedee.Color} 
 *            The light color
 */
threedee.PointLight.prototype.getColor = function()
{
    return this.color;
};
